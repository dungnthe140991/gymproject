import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {persistStore} from 'redux-persist';
import {PersistGate} from 'redux-persist/es/integration/react';
import FlashMessage from 'react-native-flash-message';

import store from './src/store/configureStore';
import Router from './src/Router';
class App extends Component {
  render() {

    console.log("STORE {}",store)


    const persistor = persistStore(store);

    console.log("AAAAAAAA {}",persistor)

    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Router />
          <FlashMessage position="top" floating={true} hideStatusBar={false} />
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
