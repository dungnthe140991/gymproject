import {showMessage} from 'react-native-flash-message';

import {
  login,
  resetPasswordWithEmail,
  getInfoPt,
  updateInfoPt,
  updatePasswordUser,
  getInfoTrainee,
  updateInfoTrainee,
} from '../api/AuthApi';
import types from './actionTypes';
import AsyncStorageService from '../api/AsyncStorageService';

export const loginWithPayload =
  (payload, meta) => async (dispatch, getState) => {
    dispatch({type: types.LOGIN});

    const json = await login(payload);

    if (json?.id_token) {
      const {role, id_token} = json;
      dispatch({type: types.LOGIN_SUCCESS, data: json});
      AsyncStorageService.putToken(id_token);
      meta.onSuccess();

      if (role.length > 0 && role[0].name === 'ROLE_PT') {
        dispatch(getInfoPT());
      } else if (role.length > 0 && role[0].name === 'ROLE_TRAINEE') {
        dispatch(getInformationTrainee());
      }
    } else {
      dispatch({type: types.LOGIN_FAILURE});
      meta.onFailure();
      if (json.error && json.errorCode === 401) {
        showMessage({
          message: 'Username or password incorrect',
          type: 'danger',
          duration: 2000,
        });
        return;
      }
      showMessage({
        message: 'Request failed. Please try again later',
        type: 'danger',
        duration: 2000,
      });
    }
  };

export const resetPassword = (email, meta) => async (dispatch, getState) => {
  // dispatch({type: types.LOGIN});

  const json = await resetPasswordWithEmail(email);
  console.log('resetPassword json', json);

  // if (json?.id_token) {
  //   dispatch({type: types.LOGIN_SUCCESS, data: json});
  //   AsyncStorageService.putToken(json.id_token);
  //   meta.onSuccess();
  // } else {
  //   dispatch({type: types.LOGIN_FAILURE});
  //   meta.onFailure();
  //   if (json.error && json.errorCode === 401) {
  //     showMessage({
  //       message: 'Username or password incorrect',
  //       type: 'danger',
  //       duration: 2000,
  //     });
  //     return;
  //   }
  //   showMessage({
  //     message: 'Request failed. Please try again later',
  //     type: 'danger',
  //     duration: 2000,
  //   });
  // }
};

export const getInfoPT = () => async (dispatch) => {
  dispatch({type: types.GETTING_PT_INFO});

  const json = await getInfoPt();
  console.log("getInfoPT: ",json)

  if (json?.id) {
    dispatch({type: types.GET_PT_INFO_SUCCESS, data: json});
  } else {
    dispatch({type: types.GET_PT_INFO_FAILURE});
  }
};

export const getInformationTrainee = () => async (dispatch) => {
  dispatch({type: types.GETTING_PT_INFO});

  const json = await getInfoTrainee();
  console.log("getInfoTrainee: ",json)
  
  if (json?.id) {
    dispatch({type: types.GET_PT_INFO_SUCCESS, data: json});
  } else {
    dispatch({type: types.GET_PT_INFO_FAILURE});
  }
};

export const logout = () => async (dispatch) => {
  await AsyncStorageService.putToken('');
  await AsyncStorageService.putRole('');
  dispatch({type: types.LOGOUT});
};

export const updateInfo = (payload, role) => async (dispatch) => {
  dispatch({type: types.UPDATING_INFO});

  let json = {};

  console.log("PAYLOAD: ",payload);

  if (role === 'ROLE_TRAINEE') {
    json = await updateInfoTrainee(payload);
  } else {
    json = await updateInfoPt(payload);
  }

  console.log('ERROR: ', json);

  await AsyncStorageService.putUser('');

  if (json?.id) {
    dispatch({type: types.UPDATE_INFO_SUCCESS, data: json});
    showMessage({
      message: 'Update user info successful',
      type: 'success',
      duration: 2000,
    });
  } else {
    dispatch({type: types.UPDATE_INFO_FAILURE});
    showMessage({
      message: 'Update user info failed. Please try again',
      type: 'danger',
      duration: 2000,
    });
  }
};

export const updatePassword = (payload, meta) => async (dispatch) => {
  const json = await updatePasswordUser(payload);

  if (json.error) {
    showMessage({
      message: 'Request failed. Please try again later',
      type: 'danger',
      duration: 2000,
    });
  } else {
    showMessage({
      message: 'Change Password success',
      type: 'success',
      duration: 2000,
    });
    meta.onSuccess();
  }
};

// const updateLocationUserSuccess = (data) => {
//   return { type: types.UPLOAD_LOCATION_USER_SUCCESS, data }
// }

// const updateLocationUserFailure = (data) => {
//   return { type: types.UPLOAD_LOCATION_USER_FAILURE, data }
// }

// export const synUserInfo = (payload, meta) => async (dispatch) => {
//   dispatch({ type: types.SYNCHRONIZING_USER_INFO })

//   const json = await apiWorker.synUserInfo(payload)

//   // if (json.status === "SUCCESS") {
//   //   const { data } = json.data
//   //   dispatch(synUserInfoSuccess(data))
//   //   meta.onSuccess()
//   // } else {
//   //   const { errors } = json.errors
//   //   dispatch(synUserInfoFailure(errors))
//   //   meta.onFailure()
//   // }
// }
// const synUserInfoSuccess = (data) => {
//   return { type: types.SYNCHRONIZED_USER_INFO_SUCCESS, data }
// }
// const synUserInfoFailure = (errors) => {
//   return { type: types.SYNCHRONIZED_USER_INFO_FAILURE, errors }
// }

// export const updateVerifyPhoneUser = (payload, meta) => async (dispatch) => {
//   dispatch({ type: types.UPDATING_VERIFY_PHONE_USER })

//   const json = await apiWorker.updateVerifyPhoneUser(payload)

//   if (json.status === 'SUCCESS') {
//     const { data } = json.data
//     dispatch(updateVerifyPhoneSuccess(data))
//     meta.onSuccess()
//   } else {
//     showMessage({
//       message: json?.message || 'Yêu cầu thất bại. Vui lòng thử lại sau',
//       type: 'danger',
//       duration: 1500,
//     })
//     dispatch(updateVerifyPhoneFailure())
//     meta.onFailure()
//   }
// }
// const updateVerifyPhoneSuccess = (data) => {
//   return { type: types.UPDATE_VERIFY_PHONE_USER_SUCCESS, data }
// }
// const updateVerifyPhoneFailure = () => {
//   return { type: types.UPDATE_VERIFY_PHONE_USER_FAILURE }
// }
