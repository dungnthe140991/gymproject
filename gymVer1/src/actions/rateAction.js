import { showMessage } from 'react-native-flash-message';

import {
  getRate,
  addNewRate,
  getRatesOfPt,
  getRateOfPtAboutRoom,
  addNewRateOfPt,
  getPtProfileAndRating,
} from '../api/RateApi';
import types from './actionTypes';
import AsyncStorageService from '../api/AsyncStorageService';

export const getRates = (meta) => async (dispatch) => {
  dispatch({ type: types.GETTING_RATE });

  const json = await getRate();
  console.log('getRates', json);
  await AsyncStorageService.putUser('');

  if (json?.rateCenterDTO?.id || json?.ratePTDTO?.id) {
    dispatch({ type: types.GET_RATE_SUCCESS, data: json });
    meta && meta.onSuccess(json);
  } else {
    dispatch({ type: types.GET_RATE_FAILURE });
  }
};

export const addNewRating = (payload) => async (dispatch) => {
  dispatch({ type: types.ADDING_RATE_OF_PT });
  const json = await addNewRate(payload);
  console.log('addNewRating', json);
  if (json?.rateCenterDTO?.id || json?.ratePTDTO?.id) {
    dispatch({ type: types.ADD_RATE_OF_PT_SUCCESS, data: json });
    meta && meta.onSuccess(json);
  } else if (json?.metaData?.message) {
    showMessage({
      message: json.metaData.message,
      type: 'warning',
      duration: 2000,
    });
    dispatch({ type: types.ADD_RATE_OF_PT_FAILURE });
  } else {
    // dispatch({ type: types.ADD_RATE_OF_PT_FAILURE });
    showMessage({
      message: "Send rating Successful",
      type: 'success',
      duration: 2000,
    });
  }
};

export const getRatesOfPT = (params) => async (dispatch) => {
  dispatch({ type: types.GETTING_RATE_OF_PT });

  const json = await getRatesOfPt(params);
  console.log('getRatesOfPT aa', json);

  if (json?.traineeRatePTResource?.content) {
    dispatch({ type: types.GET_RATE_OF_PT_SUCCESS, data: json });
    // meta && meta.onSuccess(json);
  } else {
    dispatch({ type: types.GET_RATE_OF_PT_FAILURE });
  }
};

export const getRateOfPTAboutRoom = (meta) => async (dispatch) => {
  // dispatch({type: types.GETTING_RATE_OF_PT});

  const json = await getRateOfPtAboutRoom();
  console.log('getRateOfPTAboutRoom', json);
  await AsyncStorageService.putUser('');

  // if (json?.traineeRatePTResource?.content) {
  //   dispatch({type: types.GET_RATE_OF_PT_SUCCESS, data: json});
  meta && meta.onSuccess(json);
  // } else {
  //   dispatch({type: types.GET_RATE_OF_PT_FAILURE});
  // }
};

export const addNewRateOfPT = (payload) => async (dispatch) => {
  console.log("PAYLOAD: ", payload)
  const json = await addNewRateOfPt(payload);
  if (json?.id) {
    showMessage({
      message: 'Rating room success',
      type: 'success',
      duration: 2000,
    });
  } else {
    showMessage({
      message: 'Request failed. Please try again later',
      type: 'warning',
      duration: 2000,
    });
  }
};

export const getPtProfileAndRatings = (meta) => async (dispatch) => {
  const json = await getPtProfileAndRating();
  console.log("DATA API: ", json?.namePT)
  if (json?.namePT.length > 0) {
    meta.onSuccess(json);
  }
};