import { showMessage } from 'react-native-flash-message';

import types from './actionTypes';
import {
  getExercises,
  getMyExercises,
  getExerciseById,
  addMyExercises,
  deleteExercises,
  getDoingExerciseById,
  addDetailEx,
  addDoingEx,
  deleteDetailEx,
  getDoingEx,
} from '../api/ExercisesApi';
import AsyncStorageService from '../api/AsyncStorageService';

export const getListExercises = () => async (dispatch) => {
  dispatch({ type: types.GETTING_ROOM_EXERCISES });

  const json = await getExercises();

  if (json?.content?.length >= 0) {
    dispatch({ type: types.GET_ROOM_EXERCISES_SUCCESS, data: json.content });
  } else {
    dispatch({ type: types.GET_ROOM_EXERCISES_FAILURE });
  }
};

export const getListMyExercises = () => async (dispatch) => {
  dispatch({ type: types.GETTING_MY_EXERCISES });

  const json = await getMyExercises();

  await AsyncStorageService.putUser('');
  if (json?.content?.length >= 0) {
    dispatch({ type: types.GET_MY_EXERCISES_SUCCESS, data: json.content });
  } else {
    // showMessage({
    //   message: json?.message || 'Yêu cầu thất bại. Vui lòng thử lại sau',
    //   type: 'danger',
    //   duration: 1500,
    // })
    dispatch({ type: types.GET_MY_EXERCISES_FAILURE });
  }
};

export const getDetailExercise = (id, meta) => async (dispatch) => {
  const json = await getExerciseById(id);

  console.log("ID: ", id, " DETAIL: ", json)

  if (json?.content?.length >= 0) {
    meta.onSuccess(json);
  } else {
    showMessage({
      message: json?.message || 'Request failed',
      type: 'danger',
      duration: 2000,
    });
    meta.onFailure({});
  }
};

export const getDoingExercise = (id, meta) => async (dispatch) => {
  const json = await getDoingExerciseById(id);
  console.log(json)

  if (json?.content?.length >= 0) {
    meta.onSuccess(json);
  } else {
    showMessage({
      message: json?.message || 'Request failed',
      type: 'danger',
      duration: 2000,
    });
    meta.onFailure({});
  }
};

export const addNewMyExercises = (payload, meta) => async (dispatch) => {
  dispatch({ type: types.ADDING_NEW_EXERCISES });
  const json = await addMyExercises(payload);

  await AsyncStorageService.putUser('');
  await AsyncStorageService.putRole('');

  if (json?.id) {
    dispatch({ type: types.ADD_NEW_EXERCISES_SUCCESS, data: json });
    meta.onSuccess();
  } else {
    showMessage({
      message: json?.message || 'Request failed',
      type: 'danger',
      duration: 2000,
    });
    dispatch({ type: types.ADD_NEW_EXERCISES_FAILURE });
    meta.onFailure();
  }
};

export const addDetailExercises = (payload, meta) => async (dispatch) => {
  dispatch({ type: types.ADDING_NEW_DETAIL_EXERCISES });
  const json = await addDetailEx(payload);

  if (json?.id) {
    dispatch({ type: types.ADD_NEW_DETAIL_EXERCISES_SUCCESS, data: json });
    meta.onSuccess();
  } else {
    showMessage({
      message: json?.message || 'Request failed',
      type: 'danger',
      duration: 2000,
    });
    dispatch({ type: types.ADD_NEW_DETAIL_EXERCISES_FAILURE });
    meta.onFailure();
  }
};

export const addDoingExercise = (id, payload, meta) => async (dispatch) => {
  const json = await addDoingEx(id, payload);

  if (json?.id) {
    showMessage({
      message: 'Add success',
      type: 'success',
      duration: 2000,
    });
    meta.onSuccess(json);
  } else {
    showMessage({
      message: 'Request failed',
      type: 'warning',
      duration: 2000,
    });
    meta.onFailure();
  }
};

export const getDoingExer = (id, meta) => async (dispatch) => {
  const json = await getDoingEx(id);
  console.log("JSON: ", json)
  meta.onSuccess(json);
};

export const removeExercise = (id, meta) => async (dispatch) => {
  const json = await deleteExercises(id);
  meta.onSuccess();
};
export const deleteDetailExcercise = (id, meta) => async (dispatch) => {
  const json = await deleteDetailEx(id);
  console.log("JSON: ", json)
  meta.onSuccess();
};
