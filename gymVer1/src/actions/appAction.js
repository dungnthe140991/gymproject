import {showMessage} from 'react-native-flash-message';

import types from './actionTypes';
import {uploadImageApp, getInfoAboutUs} from '../api/AppApi';

export const finishIntro = () => {
  return {type: types.FINISH_INTRO};
};

export const finishInputBMI = () => {
  return {type: types.FINISH_INPUT_BMI};
};

export const clearFetching = () => {
  return {type: types.CLEAR_FETCHING};
};

export const uploadImage = (payload, meta) => async (dispatch) => {
  const json = await uploadImageApp(payload);

  if (json?.length > 0) {
    meta.onSuccess(json);
  } else {
    meta.onFailure();
    showMessage({
      message: 'Uploading image failed',
      type: 'warning',
      duration: 2000,
    });
  }
};

export const getInformationAboutUs = () => async (dispatch) => {
  const json = await getInfoAboutUs();

  if (json?.length > 0) {
    dispatch({type: types.GET_INFO_ABOUT_US, data: json[0]});
  }
};
