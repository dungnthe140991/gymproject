const Images = {
  logo: require('../assets/images/logo.png'),
  bgIntro: require('../assets/images/intro/pexels-andrea-piacquadio-3757943.jpg'),
  bgLogin: require('../assets/images/login/bgMain.jpg'),
  defaultAvatar: require('../assets/images/avatarDefault.jpg'),

  bgExercisesAtHome: require('../assets/images/exercises/atHome.jpg'),
  bgExercisesAtGym: require('../assets/images/exercises/atGym.jpg'),
  bgExercisesAtGymWithPT: require('../assets/images/exercises/atGymWithPT.jpg'),

  abdominalsMuscles: require('../assets/images/exercises/abdominals.png'),
  backMuscles: require('../assets/images/exercises/back.png'),
  bicepsMuscles: require('../assets/images/exercises/biceps.png'),
  cardioMuscles: require('../assets/images/exercises/cardio.png'),
  chestMuscles: require('../assets/images/exercises/chest.png'),
  legsMuscles: require('../assets/images/exercises/legs.png'),
  personalMuscles: require('../assets/images/exercises/personal.png'),
  shouldersMuscles: require('../assets/images/exercises/shoulders.png'),
  tricepsMuscles: require('../assets/images/exercises/triceps.png'),

  headerDetailExercises: require('../assets/images/exercises/headerDetail.png'),
  bFBPExercises: require('../assets/images/exercises/BFBP.png'),
  bICPExercises: require('../assets/images/exercises/BICP.png'),
  cCFExercises: require('../assets/images/exercises/CCF.png'),
  cDExercises: require('../assets/images/exercises/CD.png'),
  dBPExercises: require('../assets/images/exercises/DBP.png'),
  dFExercises: require('../assets/images/exercises/DF.png'),

  step1Exercises: require('../assets/images/exercises/step1.png'),
  step2Exercises: require('../assets/images/exercises/step2.png'),
  step3Exercises: require('../assets/images/exercises/step2.png'),
  bgyoga: require('../assets/images/home/yoga.png'),
  bgcuta: require('../assets/images/home/cuta.png'),
  bgrun: require('../assets/images/home/image_run.png'),
  bgchongday: require('../assets/images/home/chongday.png'),
  bgchucmung: require('../assets/images/home/chucmung.png'),

  activeStar: require('../assets/images/report/activeStar.png'),
  inActiveStar: require('../assets/images/report/inActiveStar.png'),
};

export default Images;
