import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  leftContainer: {
    width: 60,
    zIndex: 3,
  },

  centerContainer: {},
  textTitle: {
    fontWeight: 'bold',
    fontSize: 22,
  },

  rightContainer: {
    width: 60,
    alignItems: 'flex-end',
  },
  textRightTitle: {
    fontSize: 18,
  },
});

export default styles;
