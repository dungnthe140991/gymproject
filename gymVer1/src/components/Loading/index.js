import {ActivityIndicator, View} from 'react-native';
import React, {PureComponent} from 'react';

import styles from './styles';

export default class Loading extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.layoutLoading}>
        <ActivityIndicator size="large" color="#50a1f1" />
      </View>
    );
  }
}
