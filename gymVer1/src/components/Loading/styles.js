import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  layoutLoading: {
    backgroundColor: 'rgba(0,0,0, 0.6)',
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 10,
  },
});

export default styles;
