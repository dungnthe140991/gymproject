import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    marginTop: 20,
    borderRadius: 40,
    paddingHorizontal: 20,
    paddingVertical: Constants.isIOS ? 12 : 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  borderBottomStyle: {
    borderBottomColor: Colors.primary,
    borderBottomWidth: 2,
  },

  emptyView: {
    width: 30,
  },

  iconView: {
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },

  localInputStyles: {
    flexGrow: 1,
  },

  textError: {
    paddingHorizontal: 30,
    color: Colors.error,
    fontSize: 14,
    marginTop: 5,
  },
});

export default styles;
