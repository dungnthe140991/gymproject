import React, {PureComponent} from 'react';
import {Text, View, TouchableOpacity, FlatList} from 'react-native';
import Modal from 'react-native-modal';
import FastImage from 'react-native-fast-image';
import IonIcons from 'react-native-vector-icons/Ionicons';

import styles from './styles';
import Constants from '../../common/Constants';

class ModalSelect extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
      selectedItem: null,
    };
  }

  openModal = () => {
    this.setState({isModalVisible: true});
  };
  closeModal = () => {
    this.setState({isModalVisible: false});
  };

  selectValue = (value) => {
    const {getValue, multiple} = this.props;

    if (multiple === true) {
      getValue(value);
    } else if (multiple === false) {
      this.closeModal();
      getValue(value);
    }
  };

  render() {
    const {title, items, multiple} = this.props;
    const {isModalVisible} = this.state;
    const lengthItems = items?.length || 0;
    if (multiple === true) {
      return (
        <Modal
          swipeDirection={['up', 'left', 'right', 'down']}
          isVisible={isModalVisible}
          onBackdropPress={this.closeModal}
          style={styles.modal}>
          <View
            style={[
              styles.container,
              lengthItems > 10 && {height: Constants.Window.height * 0.5},
            ]}>
            <Text style={styles.textTitle}>{title}</Text>

            {lengthItems > 0 && (
              <FlatList
                data={items}
                renderItem={({item, index}) => (
                  <View key={item.id}>
                    <TouchableOpacity
                      style={styles.buttonSelect}
                      onPress={() => this.selectValue(item)}>
                      <Text style={styles.textButtonSelect}>
                        {item.label || item.name}
                      </Text>
                      {item.isSelect && (
                        <IonIcons name="md-checkmark-sharp" size={16} />
                      )}
                    </TouchableOpacity>
                    {lengthItems !== index + 1 && (
                      <View style={styles.lineBlur} />
                    )}
                  </View>
                )}
                keyExtractor={(item) => item.id}
              />
            )}
          </View>
        </Modal>
      );
    }
    return (
      <Modal
        swipeDirection={['up', 'left', 'right', 'down']}
        isVisible={isModalVisible}
        onBackdropPress={this.closeModal}
        style={styles.modal}
        useNativeDriverForBackdrop>
        <View
          style={[
            styles.container,
            lengthItems > 10 && {height: Constants.Window.height * 0.5},
          ]}>
          <Text style={styles.textTitle}>{title}</Text>

          {lengthItems > 0 &&
            items.map((item, index) => (
              <View key={item.id}>
                <TouchableOpacity
                  style={styles.buttonSelect}
                  onPress={() => this.selectValue(item)}>
                  <Text style={styles.textButtonSelect}>
                    {item.label || item.name || item.projectName}
                  </Text>
                  {item.isSelect && (
                    <IonIcons name="md-checkmark-sharp" size={16} />
                  )}
                  {item?.image && (
                    <FastImage style={styles.itemImage} source={item.image} />
                  )}
                </TouchableOpacity>
                {lengthItems !== index + 1 && <View style={styles.lineBlur} />}
              </View>
            ))}
        </View>
      </Modal>
    );
  }
}

export default ModalSelect;
