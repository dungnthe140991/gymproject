import {StyleSheet} from 'react-native';

import Constants from '../../common/Constants';

const {ScreenWidth} = Constants.Dimension;

const styles = StyleSheet.create({
  modal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  container: {
    marginTop: 'auto',
    backgroundColor: 'white',
    paddingHorizontal: 10,
    paddingTop: 10,
    paddingBottom: 20,
  },
  textTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 5,
  },
  buttonSelect: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 10,
  },
  textButtonSelect: {
    fontSize: 16,
    opacity: 0.7,
  },
  lineBlur: {
    height: 1,
    backgroundColor: '#f0f3f4',
  },
  itemImage: {
    width: ScreenWidth(0.1),
    height: ScreenWidth(0.06),
  },
});

export default styles;
