import React from 'react';
import {TouchableWithoutFeedback, Keyboard} from 'react-native';

function HideKeyboard({children}) {
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      {children}
    </TouchableWithoutFeedback>
  );
}

export default HideKeyboard;
