/* eslint-disable react-native/no-inline-styles */
import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';

import AppNavigator, { AppStackNavigatorWithoutToken } from './navigation/index';
import IntroScreen from './screens/IntroScreen';
import InputBMI from './screens/InputBMI';
import AsyncStorageService from './api/AsyncStorageService';
import {
  clearFetching,
  getDataOnHome,
  getInfoPT,
  getInformationTrainee,
} from './actions';
import Loading from './components/Loading';
import { tempData } from './common/tempData';

class Router extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      initialized: false,
    };
  }

  componentDidMount() {
    const { clearFetching } = this.props;

    tempData.selectDate = null;
    tempData.packageDate = null;
    clearFetching();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (!prevState.initialized) {
      const { token, role } = nextProps?.auth;
      if (token) {
        if (role.length > 0 && role[0].name === 'ROLE_PT') {
          nextProps.getInfoPT();
        } else if (role.length > 0 && role[0].name === 'ROLE_TRAINEE') {
          nextProps.getInformationTrainee();
        }
        AsyncStorageService.putToken(token);
        // nextProps.getDataOnHome('ROLE_USER'); // call when login is successful
      }
      return { initialized: true };
    }

    return null;
  }

  render() {
    const {
      app,
      auth,
      isFetchingAuth,
      isFetchingExercises,
      isFetchingRate,
      isFetchingTrainee,
      isFetchingCalendar,
    } = this.props;
    const { role, info } = auth;

    if (app.finishIntro === null) {
      return <IntroScreen />;
    }
    if (
      role?.length > 0 &&
      role[0].name === 'ROLE_TRAINEE' &&
      info.first_login === 1
    ) {
      return <InputBMI />;
    }
    if (auth?.token) {
      return (
        <View style={{ flex: 1 }}>
          <AppNavigator role={role} />

          {(isFetchingExercises ||
            isFetchingAuth ||
            isFetchingRate ||
            isFetchingTrainee ||
            isFetchingCalendar) && <Loading />}
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
        <AppStackNavigatorWithoutToken />

        {isFetchingAuth && <Loading />}
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  app: state.app,
  auth: state.auth,
  isFetchingAuth: state.auth.isFetching,
  isFetchingExercises: state.exercises.isFetching,
  isFetchingRate: state.rate.isFetching,
  isFetchingTrainee: state.trainee.isFetching,
  isFetchingCalendar: state.calendar.isFetching,
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { dispatch } = dispatchProps;

  return {
    ...ownProps,
    ...stateProps,
    clearFetching: () => dispatch(clearFetching()),
    getDataOnHome: (role) => dispatch(getDataOnHome(role)),
    getInfoPT: () => dispatch(getInfoPT()),
    getInformationTrainee: () => dispatch(getInformationTrainee()),
  };
};

export default connect(mapStateToProps, undefined, mergeProps)(Router);
