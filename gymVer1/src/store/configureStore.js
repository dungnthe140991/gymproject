import {applyMiddleware, compose, createStore} from 'redux';
import thunk from 'redux-thunk';

// import reducers from '../redux/index';
import rootReducers from '../reducers';

const middleware = [
  thunk,
  // more middleware
];

const configureStore = () => {
  let store = null;

  store = compose(applyMiddleware(...middleware))(createStore)(rootReducers);

  return store;
};

export default configureStore();
