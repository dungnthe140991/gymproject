import React from 'react';
import {Platform, StatusBar} from 'react-native';
import {useIsFocused} from '@react-navigation/native';
import DeviceInfo from 'react-native-device-info';

import Constants from '../common/Constants';

export const setBarStyle = (barStyle = 'dark-content') => {
  StatusBar.setBarStyle(barStyle);
};

export const setTranslucent = (value = true) => {
  if (Platform.OS === 'android') StatusBar.setTranslucent(value);
};

export const setBackgroundColor = (background = '#ffffff') => {
  if (Platform.OS === 'android')
    StatusBar.setBackgroundColor(background, false);
};

export function FocusAwareStatusBar(props) {
  const isFocused = useIsFocused();

  return isFocused ? <StatusBar {...props} /> : null;
}

export const hasNotch = DeviceInfo.hasNotch();

export const getMarginTopHeaderBar = () => {
  let marginTop = 0;
  if (Constants.isIOS) {
    if (hasNotch) {
      marginTop = 10;
    } else {
      marginTop = 10;
    }
  } else if (hasNotch) {
    marginTop = 35;
  } else {
    marginTop = 20;
  }

  return marginTop;
};
