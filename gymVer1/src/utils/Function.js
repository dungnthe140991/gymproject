export const trimString = (string) => {
  if (string) {
    return string.trim();
  }
  return '';
};
