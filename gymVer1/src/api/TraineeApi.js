import { ApiClient } from './ApiService';

export const getPersonalTrainee = () => ApiClient.get('api/personal-trainers');

export const getTrainersOfPT = () => ApiClient.get('api/trainees-pt');

// export const getExercisesById = (trainerId) =>
//   ApiClient.get(
//     `api/trainee-exercises/getExerciseTraineeByTrainee/${trainerId}`,
//   );
export const getExercisesById = (traineeId, calendarDateFrom, calendarDateTo) =>
  ApiClient.get(
    `api/gym-calendars?traineeId=${traineeId}&calendarDateFrom=${calendarDateFrom}&calendarDateTo=${calendarDateTo}&page=0&size=1000000`,
  );

export const uppdateBmis = (payload) =>
  ApiClient.post('api/bmis', payload);

export const ptOfTraineeBeforeBooking = () =>
  ApiClient.get('api/trainees-and-pt/trainee');

export const changePT = (payload) =>
  ApiClient.post('api/trainee-and-pts/draft', payload);

  export const confirm = (payload, id) =>
  ApiClient.put(`api/gym-calendars/${id}/confirm`, payload);

  export const checkPayment = (orderId) =>
  ApiClient.get(`api/momo/check/${orderId}`);