import { ApiClient } from './ApiService';

export const getTrainersByDate = (ptId, date) =>
  ApiClient.get(`api/trainee-and-pts/getCalendarPT/${ptId}?date=${date}`);

export const assignExercises = (traineeID, categoryId) =>
  ApiClient.post(
    `api/trainee-categories/assignCategoryToTrainee?category_id=${categoryId}&trainee_id=${traineeID}`,
  );

export const getRegisterDayWithPT = () =>
  ApiClient.get(`api/gym-prices/register-days`);

export const getSlotTimeWithPT = () =>
  ApiClient.get(`api/slot-times?page=0&size=20`);

export const getPrice = (registerDays, registerDate) =>
  ApiClient.get(
    `api/gym-prices/prices?registerDays=${registerDays}&registerDate=${registerDate}`,
  );

export const getPTByParams = (slotId, registerDate) =>
  ApiClient.get(
    `api/trainees-and-pt/slot/${slotId}?register-date=${registerDate}`,
  );

export const bookingPT = (payload) =>
  ApiClient.post(`api/trainee-and-pts`, payload);

export const momoPayment = (payload) =>
  ApiClient.post(`api/momo/register/pt-and-trainee`, payload);

export const assignExercisesAndDate = (payload) =>
  ApiClient.post(`api/gym-calendars`, payload);