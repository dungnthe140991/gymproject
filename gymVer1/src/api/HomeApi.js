import {ApiClient} from './ApiService';

export const getDataOnHomeScreen = (role) =>
  ApiClient.get(`api/home?role=${role}`);

export const getListPost = () => ApiClient.get('api/news/top-lastest-news');
export const getDetailPost = (id) => ApiClient.get(`api/news/${id}`);

export const getHomeByTraineeRole = () => ApiClient.get('api/home-trainee?page=0&size=5');
