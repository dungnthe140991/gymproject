import AppConfig from '../common/AppConfig.json';
import AsyncStorageService from './AsyncStorageService';
import {ApiClient} from './ApiService';

export const uploadImageApp = async (payload) => {
  const requestOptions = {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${await AsyncStorageService.getToken()}`,
    },
    body: payload,
    redirect: 'follow',
  };

  return fetch(`${AppConfig.api.url}api/uploadImage`, requestOptions)
    .then((response) => response.text())
    .then((result) => {
      return result;
    })
    .catch((error) => {
      return error;
    });
};

export const getInfoAboutUs = () => ApiClient.get('api/aboutuses');
