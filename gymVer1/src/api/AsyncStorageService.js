import AsyncStorage from '@react-native-community/async-storage';

class AsyncStoreService {
  constructor() {
    this.TOKEN_KEY = 'token';
    this.ROLE_KEY = 'role';
    this.USER_ID = 'userId';
  }

  async putUser(id) {
    try {
      return await AsyncStorage.setItem(this.USER_ID, id);
    } catch (e) {
      console.warn('StorageClient: Failed to put user');
    }
  }
  async getUser() {
    try {
      return await AsyncStorage.getItem(this.USER_ID);
    } catch (e) {
      console.warn('StorageClient: Failed to get user');
    }
  }
  async putToken(token) {
    try {
      await AsyncStorage.setItem(this.TOKEN_KEY, token);
    } catch (e) {
      console.warn('StorageClient: Failed to put token');
    }
  }

  async getToken() {
    try {
      return await AsyncStorage.getItem(this.TOKEN_KEY);
    } catch (e) {
      console.warn('StorageClient: Failed to get token');
    }
  }

  async putRole(data) {
    try {
      await AsyncStorage.setItem(this.ROLE_KEY, data);
    } catch (e) {
      console.warn('StorageClient: Failed to put track data');
    }
  }

  async getRole() {
    try {
      return await AsyncStorage.getItem(this.ROLE_KEY);
    } catch (e) {
      console.warn('StorageClient: Failed to get track data');
    }
  }

  /**
   * Saves string to storage.
   *
   * @param key The key to fetch.
   * @param value The value to store.
   */
  async saveString(key, value) {
    try {
      await AsyncStorage.setItem(key, value);
      return true;
    } catch {
      return false;
    }
  }

  /**
   * Loads something from storage and runs it thru JSON.parse.
   *
   * @param key The key to fetch.
   */
  async load(key) {
    try {
      const almostThere = await AsyncStorage.getItem(key);
      return typeof almostThere === 'string' ? JSON.parse(almostThere) : null;
    } catch {
      return null;
    }
  }

  /**
   * Saves an object to storage.
   *
   * @param key The key to fetch.
   * @param value The value to store.
   */
  async save(key, value) {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(value));
      return true;
    } catch {
      return false;
    }
  }

  /**
   * Removes something from storage.
   *
   * @param key The key to kill.
   */
  async remove(key) {
    try {
      await AsyncStorage.removeItem(key);
    } catch {}
  }

  /**
   * Burn it all to the ground.
   */
  async clear() {
    try {
      await AsyncStorage.clear();
    } catch {
      return null;
    }
  }
}

export default new AsyncStoreService();
