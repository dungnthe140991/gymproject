import axios from 'axios';

import AsyncStorageService from './AsyncStorageService';
import AppConfig from '../common/AppConfig.json';

const APIInstant = axios.create();
const createAPI = () => {
  APIInstant.interceptors.request.use(async (config) => {
    const userId = await AsyncStorageService.getUser();
    const userRole = await AsyncStorageService.getRole();
    if (userId?.length > 0) {
      config.headers['user-id'] = +userId;
    }
    if (userRole?.length > 0) {
      config.headers['role'] = userRole;
    }
    config.baseURL = AppConfig.api.url;
    config.headers = {
      Authorization: `Bearer ${await AsyncStorageService.getToken()}`,
      'Content-Type': 'application/json',
      ...config.headers,
    };
    console.log("CONFIG:", config);
    return config;
  });
  APIInstant.interceptors.response.use(
    (response) => {         
      console.log("RESPONSE:", response); 
      return response;
    },
    (error) => {
      console.log("ERROR:", error); 
      // const message = error?.response?.data?.message;
      // showMessage({
      //   message: message,
      //   type: 'danger',
      //   duration: 1500,
      // });
      // return Promise.reject(error);
      return {
        data: {
          error: true,
          errorCode: error?.response?.status || 0,
          responseError: error.response.data,
        },
      };
    },
  );
  return APIInstant;
};
const axiosClient = createAPI();
function handleResult(api, generic) {
  console.log('API:', api)
  return api.then((res) => handleResponse(res.data));
}
function handleResponse(data) {
  return Promise.resolve(data);
}
export const ApiClient = {
  get: (url, payload) => handleResult(axiosClient.get(url, payload)),
  post: (url, payload) => handleResult(axiosClient.post(url, payload)),
  put: (url, payload) => handleResult(axiosClient.put(url, payload)),
  path: (url, payload) => handleResult(axiosClient.patch(url, payload)),
  delete: (url, payload) =>
    handleResult(axiosClient.delete(url, {data: payload})),
};
