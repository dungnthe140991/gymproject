import types from '../actions/actionTypes';

const initialState = {
  isFetching: false,
  data: {},
  dataOfPT: {},
};

const rateReducer = (state = initialState, action) => {
  const {type, data} = action;

  switch (type) {
    case types.GETTING_RATE:
      return {
        ...state,
        isFetching: true,
      };
    case types.GET_RATE_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: data,
      };
    case types.GET_RATE_FAILURE:
      return {
        ...state,
        isFetching: false,
        data: {},
      };

    case types.GETTING_RATE_OF_PT:
      return {
        ...state,
        isFetching: true,
      };
    case types.GET_RATE_OF_PT_SUCCESS:
      return {
        ...state,
        isFetching: false,
        dataOfPT: data,
      };
    case types.GET_RATE_OF_PT_FAILURE:
      return {
        ...state,
        isFetching: false,
        dataOfPT: {},
      };

    case types.CLEAR_FETCHING:
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
};

export default rateReducer;
