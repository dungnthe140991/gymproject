import types from '../actions/actionTypes';

const initialState = {
  finishIntro: null,
  finishInputBMI: null,
  isFetching: false,
  appInfo: null,
};

const appReducer = (state = initialState, action) => {
  const {type, data} = action;

  switch (type) {
    case types.FINISH_INTRO:
      return {...state, finishIntro: true};

    case types.FINISH_INPUT_BMI:
      return {...state, finishInputBMI: true};

    case types.GET_INFO_ABOUT_US:
      return {
        ...state,
        appInfo: data,
      };

    default:
      return state;
  }
};

export default appReducer;
