import { StyleSheet } from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const { ScreenWidth } = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },

  imageHeaderView: {
    width: ScreenWidth(1),
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -10,
  },
  imageHeader: {
    width: ScreenWidth(0.77),
    height: ScreenWidth(0.1),
  },

  traineeInfoView: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    marginVertical: 15,
  },
  dateFromTo: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '70%',
    marginLeft: '5%',
    marginBottom: '2%'
  },
  dateFromToL: {
    flexDirection: 'row',
    marginLeft: '13%',
    marginBottom: '2%',
  },
  dateFromToR: {
    flexDirection: 'row',
    marginLeft: '35%',
    marginBottom: '2%',
  },
  textDateBorder: {
    borderBottomColor: 'blue',
    borderBottomWidth: 2,
  },
  textDateBorder1: {
    fontWeight: 'bold',
  },
  emptyInfoView: {
    width: '30%',
  },
  nameInfoView: {
    marginLeft: '10%',
    width: '55%',
  },
  nameInfoText: {
    fontWeight: 'bold',
  },
  bMIInfoView: {
    width: '25%',
  },
  bMIInfoText: {
    color: 'red',
    fontWeight: 'bold',
  },

  headerContainer: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: Colors.sectionSeparatorColor,
    paddingVertical: 12,
  },
  sttTitleTable: {
    width: '20%',
    alignItems: 'center',
  },
  nameTitleTable: {
    width: 80,
    alignItems: 'center',
  },
  exercise: {
    width: 150,
    alignItems: 'center',
  },
  attended: {
    width: 80,
    alignItems: 'center',
  },
  textTitleTable: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  listRoomExercises: {
    height: '100%',
    width: '96%',
    flex: 1,
    // flexGrow: 0,
    marginTop: 10,
    marginLeft: 5,
  },
  item: {
    flexDirection: 'row',
    marginVertical: 15,
  },
  sttView: {
    width: '20%',
    alignItems: 'center',
  },
  nameView: {
    width: 75,
    alignItems: 'center',
  },
  phoneView: {
    width: 80,
    marginLeft: 28,
  },
  itemTextExTitle: {
    color: Colors.blue,
    fontWeight: 'bold',
  },

  itemSeparatorComponent: {
    height: 0.5,
    width: '100%',
    backgroundColor: Colors.border,
  },

  containerPlusButton: {
    width: '100%',
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  plusButton: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: Colors.lightBlue,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  addDoingIcon: {
    fontSize: 24,
    color: Colors.white,
  },
});

export default styles;
