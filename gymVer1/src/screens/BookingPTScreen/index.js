import React, { useRef, useState, useEffect } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Linking, Button, ScrollView, Alert } from 'react-native';
import FastImage from 'react-native-fast-image';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import SelectDropdown from 'react-native-select-dropdown';
import { showMessage } from 'react-native-flash-message';
import LinearGradient from 'react-native-linear-gradient';
import IIcons from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import { DataTable } from 'react-native-paper';

import styles from './styles';
import Images from '../../common/Images';
import Colors from '../../common/Colors';
import HeaderActionScreen from '../../components/HeaderActionScreen';
import InputCustom from '../../components/InputCustom';
import Dialog, { SlideAnimation, DialogContent } from 'react-native-popup-dialog';

import {
  getRegisterDayWithPt,
  getPriceByParams,
  getSlotTimeWithPt,
  getPtByParams,
  bookingPt,
  getPtOfTraineeBeforeBooking,
  changePt,
  momo,
  checkMomoPayment,
} from '../../actions';
import { tempData } from '../../common/tempData';

const BookingPTScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const route = useRoute();

  const [timeRegister, setTimeRegister] = useState([]);
  const [slot, setSlot] = useState([]);
  const [pt, setPT] = useState([]);
  const [price, setPrice] = useState('');
  const [date, setDate] = useState('');
  const [selectedPT, setSelectedPT] = useState(null);
  const [selectedSlot, setSelectedSlot] = useState(null);
  const [orderId, setOrderId] = useState(null);
  const refReason = useRef(null);

  const [packageSe, setpackageSe] = useState("Package");
  const [slotSe, setSlotSe] = useState("Slot");
  const [ptSe, setPtSe] = useState("PT");
  const [shouldShow, setShouldShow] = useState(true);

  const [ptOfTrainee, setPtOfTrainee] = useState(null);


  // useEffect(() => {
  //   dispatch(
  //     getRegisterDayWithPt({
  //       onSuccess: (data) => {
  //         setTimeRegister(data);
  //       },
  //     }),
  //   );
  //   dispatch(
  //     getSlotTimeWithPt({
  //       onSuccess: (data) => {
  //         setSlot(data);
  //       },
  //     }),
  //   );
  // }, []);

  useEffect(() => {
    dispatch(
      getRegisterDayWithPt({
        onSuccess: (data) => {
          setTimeRegister(data);
        },
      }),
    );
    dispatch(
      getSlotTimeWithPt({
        onSuccess: (data) => {
          setSlot(data);
        },
      }),
    );
    const unsubscribe = navigation.addListener('focus', () => {
      setDate(tempData.selectDate);
      console.log("SLOT: ", slot)
      dispatch(
        getPtOfTraineeBeforeBooking({
          onSuccess: (data) => {
            setPtOfTrainee(data);
          },
        }),
      );
    });
    return unsubscribe;
  }, [navigation]);

  const navCalendar = () => {
    navigation.navigate('CalenderScreen', { from: 'ChangePTScreen' });
  };

  const onSelectItem = (item) => {
    if (tempData.selectDate) {
      setpackageSe(item)
      tempData.packageDate = item;
      dispatch(
        getPriceByParams(item, tempData.selectDate, {
          onSuccess: (data) => {
            setPrice(data);
          },
        }),
      );
    } else {
      showMessage({
        message: 'Please choose date!!',
        type: 'warning',
        duration: 2000,
      });
    }
  };

  const onSelectSlotItem = (item) => {
    console.log('onSelectSlotItem', item);
    if (tempData.selectDate) {
      setSlotSe(item.name);
      setSelectedSlot(item.id);
      dispatch(
        getPtByParams(item.id, tempData.selectDate, {
          onSuccess: (data) => {
            setPT(data);
          },
        }),
      );
    } else {
      showMessage({
        message: 'Please choose date!!',
        type: 'warning',
        duration: 2000,
      });
    }
  };
  const onSelectSlotItemChangePT = (item) => {
    var date = moment(new Date()).format('YYYY-MM-DD');
    if (date) {
      setSlotSe(item.name);
      setSelectedSlot(item.id);
      dispatch(
        getPtByParams(item.id, date, {
          onSuccess: (data) => {
            setPT(data);
          },
        }),
      );
    } else {
      showMessage({
        message: 'Please choose date!!',
        type: 'warning',
        duration: 2000,
      });
    }
  };

  const onSelectPTItem = (item) => {
    console.log('onSelectPTItem', item);
    setPtSe(item.full_name);
    setSelectedPT(item.id);
  };

  const bookPT = () => {
    if (
      selectedPT &&
      selectedSlot &&
      price &&
      tempData.packageDate &&
      tempData.selectDate
    ) {
      const payload = {
        ptId: selectedPT,
        slotId: selectedSlot,
        price: price,
        dateRegister: tempData.packageDate,
        registerDate: tempData.selectDate,
      };
      console.log("PAYLOAD BOOKING: ", payload)
      dispatch(bookingPt(payload));
    } else {
      showMessage({
        message: 'Please select all data!!',
        type: 'warning',
        duration: 2000,
      });
    }
  };
  const bookPTAndPayment = () => {
    if (
      selectedPT &&
      selectedSlot &&
      price &&
      tempData.packageDate &&
      tempData.selectDate
    ) {
      const payload = {
        ptId: selectedPT,
        slotId: selectedSlot,
        amount: price,
        redirectUrl: 'mygymapp://booking',
      };
      console.log("PAYLOAD_MOMO: ", payload)
      dispatch(
        momo(payload, {
          onSuccess: (data) => {
            console.log('RESPONSE_MOMO: ', data)
            setOrderId(data.orderId);
            const url = data.payUrl;
            Linking.openURL(url);
          },
        }),
      );

    } else {
      showMessage({
        message: 'Please select all data!!',
        type: 'warning',
        duration: 2000,
      });
    }
  };

  const checkPayment = () => {
    const payload = {
      ptId: selectedPT,
      slotId: selectedSlot,
      price: price,
      dateRegister: tempData.packageDate,
      registerDate: tempData.selectDate,
      transactionId: orderId,
    };
    console.log("PAYLOAD BOOKING: ", payload)
    dispatch(
      checkMomoPayment(orderId === null ? 0 : orderId, payload),
    );
  };


  const traineeAndPT = () => {
    if (ptOfTrainee != null) {
      let slotTime = '';
      for (let i = 0; i < slot.length - 1; i++) {
        if (ptOfTrainee.slotId == slot[i].id) {
          slotTime = slot[i].name;
          break;
        }
      }
      // return <View style={styles.contentPT}>
      //   <Text>Registered Date: {ptOfTrainee.registeredDate}</Text>
      //   <Text>Expired Date: {ptOfTrainee.expiredDate}</Text>
      //   <Text>Slot: {slotTime}</Text>
      //   <Text>Price: {ptOfTrainee.price}</Text>
      //   <Text>Package: {ptOfTrainee.type} days</Text>
      // </View>;
      return <DataTable>
        <DataTable.Row>
          <DataTable.Cell><Text style={styles.font}>PT</Text></DataTable.Cell>
          <DataTable.Cell>{ptOfTrainee.ptName}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell><Text style={styles.font}>Package</Text></DataTable.Cell>
          <DataTable.Cell>{ptOfTrainee.type}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell><Text style={styles.font}>Slot</Text></DataTable.Cell>
          <DataTable.Cell>{slotTime}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell><Text style={styles.font}>Price</Text></DataTable.Cell>
          <DataTable.Cell>{ptOfTrainee.price}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell><Text style={styles.font}>Registered Date</Text></DataTable.Cell>
          <DataTable.Cell>{moment(ptOfTrainee.registeredDate).format('DD-MM-YYYY')}</DataTable.Cell>
        </DataTable.Row>
        <DataTable.Row>
          <DataTable.Cell><Text style={styles.font}>Expired Date</Text></DataTable.Cell>
          <DataTable.Cell>{moment(ptOfTrainee.expiredDate).format('DD-MM-YYYY')}</DataTable.Cell>
        </DataTable.Row>
      </DataTable>
    }
    return null;
  };
  const alertBeforChangeDate = (item) => {
    Alert.alert(
      "Are your sure?",
      "Do you really want to change PT?",
      [
        // The "Yes" button
        {
          text: "Yes",
          onPress: () => {
            changePtOnpress()
          },
        },
        // The "No" button
        // Does nothing but dismiss the dialog when tapped
        {
          text: "No",
        },
      ]
    );
  }
  const changePtOnpress = () => {
    if (selectedSlot == null) {
      showMessage({
        message: "Please select slot",
        type: 'warning',
        duration: 2000,
      });
    } else if (selectedPT == null) {
      showMessage({
        message: "Please select PT",
        type: 'warning',
        duration: 2000,
      });
    } else if (refReason.current.value == "") {
      showMessage({
        message: "Please enter reason",
        type: 'warning',
        duration: 2000,
      });
    } else {
      const payload = {
        traineeAndPtId: ptOfTrainee.traineeAndPtId,
        slotId: selectedSlot,
        ptId: selectedPT,
        reasonChange: refReason.current.value,
      };
      console.log('CHANGE: ', payload);
      dispatch(changePt(payload));
    }
  }

  // CALL KHI BOOKING (CHUA CO PT)
  const bookingPT = () => {
    return <SafeAreaView style={styles.container}>
      <HeaderActionScreen
        title="Booking PT"
        openDrawer={() => navigation.openDrawer()}
        sizeIconDrawer={20}
      />
      <View style={styles.imageHeaderView}>
        <FastImage
          style={styles.imageHeader}
          source={Images.headerDetailExercises}
        />
      </View>
      <View style={styles.content}>
        <View style={styles.headerContent}>
          <TouchableOpacity style={styles.inputContainer} onPress={navCalendar}>
            <Text style={styles.textPrice}>
              {date?.length > 0 ? date : 'Date'}
            </Text>
          </TouchableOpacity>

          <SelectDropdown
            data={timeRegister}
            onSelect={(selectedItem) => {
              onSelectItem(selectedItem);
            }}
            buttonTextAfterSelection={(selectedItem) => {
              return selectedItem;
            }}
            rowTextForSelection={(item) => {
              return item;
            }}
            renderCustomizedButtonChild={(selectedItem) => {
              return (
                <View style={styles.viewSelect}>
                  <Text style={styles.textSelect}>{packageSe}</Text>
                  <IIcons name="caret-down-sharp" style={styles.iconSelect} />
                </View>
              );
            }}
            buttonStyle={styles.buttonSelect}
          />

          <View style={styles.inputPrice}>
            <Text style={styles.textPrice}>{price}</Text>
          </View>
        </View>

        <View style={styles.bottomContent}>
          <SelectDropdown
            data={slot}
            onSelect={(selectedItem) => {
              onSelectSlotItem(selectedItem);
            }}
            buttonTextAfterSelection={(selectedItem) => {
              return selectedItem;
            }}
            rowTextForSelection={(item) => {
              return item.name;
            }}
            renderCustomizedButtonChild={(selectedItem) => {
              return (
                <View style={styles.viewSelect}>
                  <Text style={styles.textSelect}>{slotSe}</Text>
                  <IIcons name="caret-down-sharp" style={styles.iconSelect} />
                </View>
              );
            }}
            buttonStyle={styles.bottomButtonSelect}
          />

          <SelectDropdown
            data={pt}
            onSelect={(selectedItem) => {
              onSelectPTItem(selectedItem);
            }}
            buttonTextAfterSelection={(selectedItem) => {
              return selectedItem;
            }}
            rowTextForSelection={(item) => {
              return item.full_name;
            }}
            renderCustomizedButtonChild={(selectedItem) => {
              return (
                <View style={styles.viewSelect}>
                  <Text style={styles.textSelect}>{ptSe}</Text>
                  <IIcons name="caret-down-sharp" style={styles.iconSelect} />
                </View>
              );
            }}
            buttonStyle={styles.bottomButtonSelect}
          />
        </View>
      </View>

      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.containerPlusButton}
        onPress={bookPT}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1 }}
          colors={Colors.mainGradientButton}
          style={styles.linearGradient}>
          <Text style={styles.addText}>Send Request</Text>
        </LinearGradient>
      </TouchableOpacity>
      <View style={styles.parentButton}>
        <Text style={styles.noteText}>Must click "Check Payment" button after payment</Text>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.containerPlusButton}
          onPress={bookPTAndPayment}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 0, y: 1 }}
            colors={Colors.mainGradientButton}
            style={styles.linearGradient}>
            <Text style={styles.addText}>Booking and Payment</Text>
          </LinearGradient>
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.containerPlusButton}
          onPress={checkPayment}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 0, y: 1 }}
            colors={Colors.mainGradientButton}
            style={styles.linearGradient}>
            <Text style={styles.addText}>Check Payment</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  }
  // CALL KHI CHANGE PT (DA CO PT)
  const changePT = () => {
    return <SafeAreaView style={styles.container}>
      <HeaderActionScreen
        title=""
        openDrawer={() => navigation.openDrawer()}
        sizeIconDrawer={20}
      />
      <View style={styles.imageHeaderView}>
        <FastImage
          style={styles.imageHeader}
          source={Images.headerDetailExercises}
        />
      </View>

      <ScrollView>
        <View>
          <Text style={styles.currentPT}>Your current PT</Text>
        </View>

        {traineeAndPT()}

        <View>
          <View>
            <Text style={styles.currentPT}>Request to change PT</Text>
          </View>
          <View style={styles.bottomContent}>
            <SelectDropdown
              data={slot}
              onSelect={(selectedItem) => {
                onSelectSlotItemChangePT(selectedItem);
              }}
              buttonTextAfterSelection={(selectedItem) => {
                return selectedItem;
              }}
              rowTextForSelection={(item) => {
                return item.name;
              }}
              renderCustomizedButtonChild={(selectedItem) => {
                return (
                  <View style={styles.viewSelect}>
                    <Text style={styles.textSelect}>{slotSe}</Text>
                    <IIcons name="caret-down-sharp" style={styles.iconSelect} />
                  </View>
                );
              }}
              buttonStyle={styles.bottomButtonSelect}
            />
            <SelectDropdown
              data={pt}
              onSelect={(selectedItem) => {
                onSelectPTItem(selectedItem);
              }}
              buttonTextAfterSelection={(selectedItem) => {
                return selectedItem;
              }}
              rowTextForSelection={(item) => {
                return item.full_name;
              }}
              renderCustomizedButtonChild={(selectedItem) => {
                return (
                  <View style={styles.viewSelect}>
                    <Text style={styles.textSelect}>{ptSe}</Text>
                    <IIcons name="caret-down-sharp" style={styles.iconSelect} />
                  </View>
                );
              }}
              buttonStyle={styles.bottomButtonSelect}
            />
          </View>
          <View>
            <InputCustom
              id="reason"
              ref={refReason}
              placeholder="Enter your reason"
              containerStyleProps={styles.inputContainerr}
              useFocusInput
              hideIconLeft
            />
          </View>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.containerPlusButton}
            onPress={alertBeforChangeDate}>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 0, y: 1 }}
              colors={Colors.mainGradientButton}
              style={styles.linearGradient}>
              <Text style={styles.addText}>Change PT</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  };

  const checkCallBookingOrChangePT = () => {
    if (ptOfTrainee?.error) {
      return bookingPT();
    }
    return changePT();
  };

  return (
    checkCallBookingOrChangePT()
  );
};

export default BookingPTScreen;
