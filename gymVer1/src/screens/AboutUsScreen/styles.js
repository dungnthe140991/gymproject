import {StyleSheet} from 'react-native';

import Constants from '../../common/Constants';
import Colors from '../../common/Colors';

const {ScreenWidth} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },

  imageLogoView: {
    width: '100%',
    alignItems: 'center',
    marginBottom: 20,
    marginTop: -20,
  },
  imageLogo: {
    marginTop: 40,
    marginBottom: 10,
    width: ScreenWidth(0.6),
    height: ScreenWidth(0.42),
  },

  content: {
    paddingHorizontal: 20,
  },
  text: {
    fontSize: 17,
    fontWeight: 'bold',
    marginTop: 15,
  },
});

export default styles;
