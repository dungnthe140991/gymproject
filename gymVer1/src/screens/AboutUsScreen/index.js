/* eslint-disable react-hooks/exhaustive-deps */
import {Text, View, SafeAreaView} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import FastImage from 'react-native-fast-image';

import styles from './styles';
import Images from '../../common/Images';
import HeaderActionScreen from '../../components/HeaderActionScreen';

const AboutUsScreen = () => {
  const navigation = useNavigation();
  const appReducer = useSelector((state) => state.app);

  const {appInfo} = appReducer;

  return (
    <SafeAreaView style={styles.container}>
      <HeaderActionScreen
        title="About Us"
        openDrawer={() => navigation.openDrawer()}
        sizeIconDrawer={20}
      />
      <View style={styles.imageLogoView}>
        <FastImage source={Images.logo} style={styles.imageLogo} />
      </View>

      <View style={styles.content}>
        <Text style={styles.text}>{appInfo?.content}</Text>
        <Text style={styles.text}>
          Date of incorporation: {appInfo?.date_incorporation}
        </Text>
      </View>
    </SafeAreaView>
  );
};

export default AboutUsScreen;
