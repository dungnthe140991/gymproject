/* eslint-disable react-hooks/exhaustive-deps */
import {
  Text,
  View,
  ScrollView,
  FlatList,
  SafeAreaView,
  Image,
  TouchableOpacity,
} from 'react-native';
import React, { useEffect, useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import moment from 'moment';
import { useSelector } from 'react-redux';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FAIcons from 'react-native-vector-icons/FontAwesome';
import FA5Icons from 'react-native-vector-icons/FontAwesome5';
import IonIcons from 'react-native-vector-icons/Ionicons';
import { useDispatch } from 'react-redux';
import { getPost, getPosts } from '../../actions';
import styles from './style';
import Images from '../../common/Images';
import {
  getDetail,
  getTrainersOfPt,
  getHomeByTrainee,
  getInformationAboutUs,
} from '../../actions';

const HomeScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const authReducer = useSelector((state) => state.auth);
  const traineeReducer = useSelector((state) => state.trainee);
  const homeReducer = useSelector((state) => state.home);

  const { info, role } = authReducer;
  const { totalTrainers } = traineeReducer;
  const { dataOnHomeOfTrainee } = homeReducer;

  const [listData, setListDataNews] = useState([]);
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      dispatch(getPost(setListData));
      getInfo();
      dispatch(getInformationAboutUs());
      dispatch(getTrainersOfPt());
    });
    return unsubscribe;
  }, [navigation]);

  const getInfo = async () => {
    if (role?.length > 0 && role[0].name === 'ROLE_PT') {
      dispatch(getTrainersOfPt());
    } else {
      dispatch(getHomeByTrainee());
    }
  };

  const setListData = (data) => {
    setListDataNews(data);
  };

  const goToTypeOfExercises = (item) => {
    dispatch(
      getDetail(item.id, {
        onSuccess: (data) => {
          navigation.navigate('DetailRoomExerciseScreen', {
            data,
            showButtonAdd: false,
          });
        },
        onFailure: () => { },
      }),
    );
  };

  const goToTrainersScreen = () => {
    navigation.navigate('TrainersScreen');
  };
  const goToPTRatingScreen = () => {
    navigation.navigate('MyPtProfile');
  };
  const goToCalendarScreen = () => {
    navigation.navigate('Calendar');
  };

  function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  const renderInfo = () => {
    if (role?.length > 0 && role[0].name === 'ROLE_PT') {
      return (
        <View style={styles.rightCom}>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={goToTrainersScreen}
            style={styles.topText}>
            <View style={styles.containerText}>
              <FA5Icons name={'user-friends'} size={25} color={'green'} />
              <Text style={styles.textProcess}>Trainees: {totalTrainers}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={goToCalendarScreen}
            style={styles.bottomText}>
            <View style={styles.containerText}>
              <AntDesign name={'calendar'} size={35} color={'blue'} />
              <Text style={styles.textProcess}>Calendar</Text>
            </View>
          </TouchableOpacity>
        </View>
      );
    } else if (role?.length > 0 && role[0].name === 'ROLE_TRAINEE') {
      const { bmiResource } = dataOnHomeOfTrainee;
      return (
        <View style={styles.topText}>
          <View style={styles.containerTraineeText}>
            <View style={styles.traineeInfoView}>
              <IonIcons name="body-outline" size={25} color={'green'} />
              <Text style={styles.textTraineeProcess}>
                {bmiResource?.height}
              </Text>
            </View>
            <View style={styles.traineeInfoView}>
              <IonIcons name="md-timer-outline" size={25} color={'green'} />
              <Text style={styles.textTraineeProcess}>
                {bmiResource?.weight}
              </Text>
            </View>
            <View style={styles.traineeInfoView}>
              <IonIcons name="barbell-outline" size={25} color={'green'} />
              <Text style={styles.textTraineeProcess}>Bmi: {bmiResource?.bmi}</Text>
            </View>
          </View>

          <TouchableOpacity
            activeOpacity={0.9}
            onPress={goToPTRatingScreen}
            style={styles.bottomText2}>
            <View style={styles.containerText}>
              <MaterialCommunityIcons name={'human-child'} size={35} color={'blue'} />
              <Text style={styles.textProcess}>PT Profile</Text>
            </View>
          </TouchableOpacity>
        </View>
      );
    } else {
      return null;
    }
  };

  const renderNews = () => {
    return (
      <FlatList
        data={listData}
        renderItem={renderItem}
        keyExtractor={(item) => `${item.id}`}
        horizontal
        showsHorizontalScrollIndicator={false}
      />
    );
  };

  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => goToTypeOfExercises(item)}
        style={[
          styles.item,
          {
            // backgroundColor: getRandomColor(),
            backgroundColor: '#000000',
          },
        ]}>
        <View style={styles.itemContain}>
          <Text numberOfLines={2} style={styles.textTitle}>
            {item?.title}
          </Text>
          {/* <Text
            numberOfLines={1}
            style={styles.text}>{`${item?.exercises} exercises`}</Text> */}
          <Text numberOfLines={1} style={styles.textTime}>
            {/* {item?.time} */}
            {/* {item?.created_date} */}
            {moment(item?.created_date).format('DD/MM/YYYY')}

          </Text>
        </View>
        <View style={{ width: '50%', height: '100%' }}>
          <Image
            resizeMode="contain"
            style={{ width: '100%', height: '100%' }}
            source={{ uri: item.image }}
          />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <View style={styles.headerContain}>
          <View>
            <Text style={styles.nameHeaderText}>Hello, {info?.full_name}</Text>
            <View style={styles.activityView}>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => navigation.openDrawer()}>
                <FAIcons name="navicon" size={20} />
              </TouchableOpacity>
              <Text style={styles.activityText}>Let's check your activity</Text>
            </View>
          </View>
          <View style={styles.avatarView}>
            <View style={styles.avatar}>
              {info?.avatar ? (
                <Image
                  resizeMode="cover"
                  style={styles.avatarImage}
                  source={{ uri: info.avatar }}
                />
              ) : (
                <Image
                  resizeMode="cover"
                  style={styles.avatarImage}
                  source={Images.defaultAvatar}
                />
              )}
            </View>
          </View>
        </View>
        <View style={styles.dayContainer}>
          <View style={{ width: '50%', height: 200 }}>
            <View style={styles.leftText}>
              <View style={{ flexDirection: 'row', marginTop: 20 }}>
                <Entypo name={'calendar'} size={25} color={'red'} />
                <Text style={styles.calendar}>Calendar</Text>
              </View>
              <View>
                <Text style={{ fontSize: 80 }}>
                  {moment().toDate().getDate()}
                </Text>
              </View>
              <Text style={{ fontSize: 15 }}>Completed Workouts</Text>
            </View>
          </View>
          <View
            style={{
              width: '50%',
              height: 200,
              alignItems: 'flex-end',
            }}>
            {renderInfo()}
          </View>
        </View>
        <View
          style={{
            flex: 1,
          }}>
          <Text
            style={{
              marginLeft: 20,
              marginTop: 20,
              fontSize: 18,
              fontWeight: 'bold',
            }}>
            Discover new workouts!
          </Text>
          {renderNews()}
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={styles.footerContainer}>
            <View style={{ width: '20%', height: '100%', paddingHorizontal: 5 }}>
              <Image
                resizeMode="contain"
                style={{ width: '100%', height: '100%' }}
                source={Images.bgchucmung}
              />
            </View>
            <View style={{ flex: 1, paddingHorizontal: 20 }}>
              <Text style={{ fontSize: 16, fontWeight: 'bold' }}>
                Keep the procgress!
              </Text>
              <Text>You are more succressful than 88% users</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;
