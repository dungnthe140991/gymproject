import React, { useState, useEffect } from 'react';
import { Text, View, TouchableOpacity, FlatList, ScrollView } from 'react-native';
import FastImage from 'react-native-fast-image';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import moment from 'moment';

import styles from './styles';
import Images from '../../common/Images';
import HeaderActionScreen from '../../components/HeaderActionScreen';
import { getRates, getRatesOfPT, getPtProfileAndRatings } from '../../actions';
import AsyncStorageService from '../../api/AsyncStorageService';

const listStars = [1, 2, 3, 4, 5];

const MyPtProfile = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const rateReducer = useSelector((state) => state.rate);
  const authReducer = useSelector((state) => state.auth);

  const { role, info } = authReducer;

  const { ratePTDTO, rateCenterDTO } = rateReducer.data;
  const { dataOfPT } = rateReducer;

  const [rating, setRating] = useState(4);
  const [data, setData] = useState([]);

  useEffect(() => {
    getRate();
  }, []);

  const getRate = async () => {
    dispatch(getPtProfileAndRatings({
      onSuccess: (json) => {
        setData(json)
      },
      onFailure: () => {
      },
    }));
  };

  const renderItemReport = ({ item }) => {
    return (
      <View style={styles.reportItem}>
        <View style={styles.headerItem}>
          <FastImage source={Images.defaultAvatar} style={styles.userImgItem} />
          <Text style={styles.userNameItem}>Học viên ẩn danh</Text>
        </View>
        <View style={styles.starsItem}>
          {listStars.map((star) => (
            <View key={star} style={styles.starItem}>
              <View key={star}>
                <FastImage
                  source={
                    item.starRate >= star
                      ? Images.activeStar
                      : Images.inActiveStar
                  }
                  style={styles.starImageItem}
                />
              </View>
            </View>
          ))}
        </View>
        <Text style={styles.createAtText}>{moment(item?.created_date).format('DD/MM/YYYY')}</Text>
        <Text style={styles.contentItem}>{item.comment}</Text>
      </View>
    );
  };
  const { startRate, traineeRatePTResource } = dataOfPT;
  return (
    <View style={styles.container}>
      <HeaderActionScreen
        title="PT Profile"
        openDrawer={() => navigation.openDrawer()}
        sizeIconDrawer={20}
      />
      <FastImage
        source={{ uri: data.avatarPT }}
        style={styles.ptPicture}
      />
      <Text style={styles.ptPhone}>
        Name: {data.namePT}
      </Text>
      <Text style={styles.ptName}>
        Phone: {data.phone}
      </Text>
      <ScrollView>
        <View style={styles.wrapStar}>
          <View style={styles.stars}>
            {listStars.map((star) => (
              <View key={star} style={styles.star}>
                <View key={star}>
                  <FastImage
                    source={
                      data.startRate >= star ? Images.activeStar : Images.inActiveStar
                    }
                    style={styles.starImage}
                  />
                </View>
              </View>
            ))}
            <Text>
              {data.startRate && (Math.round(data.startRate * 100) / 100).toFixed(1)}
            </Text>
          </View>
        </View>
        <Text style={styles.ptName}>
          Trainees Rating
        </Text>
        {data.traineeRatePTResource?.content && (
          <FlatList
            style={styles.listReports}
            data={data.traineeRatePTResource.content}
            keyExtractor={(item, indx) => `${indx}`}
            renderItem={renderItemReport}
            showsVerticalScrollIndicator={false}
          />
        )}
      </ScrollView>
    </View>
  );
};

export default MyPtProfile;
