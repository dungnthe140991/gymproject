import {
  Text,
  TouchableOpacity,
  SafeAreaView,
  ImageBackground,
  View,
  Image,
} from 'react-native';
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useRef, useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import styles from './styles';
import Images from '../../common/Images';
import InputCustom from '../../components/InputCustom';
import HideKeyboard from '../../components/HideKeyboard';
import { uppdateBmi } from '../../actions';

const InputBMI = () => {
  const dispatch = useDispatch();

  const refWidth = useRef(null);
  const refHeight = useRef(null);

  const [weightBMI, setWeightBMI] = useState(0);
  const [heightBMI, setHeightBMI] = useState(0);
  const [resultBMI, setResultBMI] = useState(0);

  useEffect(() => {
    if (weightBMI > 0 && heightBMI > 0) {
      calculatorBMI();
    }
  }, [weightBMI, heightBMI]);

  const calculatorBMI = () => {
    const num = (weightBMI * 100 * 100) / (heightBMI * heightBMI);
    const result = (Math.round(num * 100) / 100).toFixed(1);
    setResultBMI(result);
  };
  const saveBMI = () => {
    const payload = {
      weight: weightBMI,
      height: heightBMI,
      bmi_number: resultBMI,
    };
    dispatch(uppdateBmi(payload));
  };

  return (
    <HideKeyboard>
      <SafeAreaView style={styles.container}>
        <ImageBackground style={styles.background} source={Images.bgIntro}>
          <View style={styles.overlay} />
          <Image source={Images.logo} style={styles.logo} />

          <Text style={styles.BMITitle}>BMI Index</Text>

          <View style={styles.inputView}>
            <Text style={styles.inputText}>Weight (Kg): </Text>
            <InputCustom
              id="weight"
              ref={refWidth}
              containerStyleProps={styles.inputContainer}
              useFocusInput
              hideIconLeft
              valueInput={weightBMI}
              setValueParent={setWeightBMI}
              keyboardType="decimal-pad"
            />
          </View>

          <View style={styles.inputView}>
            <Text style={styles.inputText}>Height (cm): </Text>
            <InputCustom
              id="height"
              ref={refHeight}
              containerStyleProps={styles.inputContainer}
              useFocusInput
              hideIconLeft
              valueInput={heightBMI}
              setValueParent={setHeightBMI}
              keyboardType="decimal-pad"
            />
          </View>

          <View style={styles.BMIView}>
            <Text style={styles.BMIText}>BMI: {resultBMI}</Text>
          </View>

          <TouchableOpacity
            style={styles.btn}
            activeOpacity={0.85}
            onPress={saveBMI}>
            <Text style={styles.btnText}>Save Change</Text>
          </TouchableOpacity>
        </ImageBackground>
      </SafeAreaView>
    </HideKeyboard>
  );
};

export default InputBMI;
