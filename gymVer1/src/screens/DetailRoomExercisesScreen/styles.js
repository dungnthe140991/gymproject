import { StyleSheet } from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const { ScreenWidth } = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },

  searchInput: {
    paddingHorizontal: 10,
    fontSize: 17,
  },

  bgGray: {
    backgroundColor: Colors.DirtyBackground,
  },

  imageHeaderView: {
    width: ScreenWidth(1),
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -10,
  },
  imageHeader: {
    width: ScreenWidth(0.77),
    height: ScreenWidth(0.1),
  },

  listRoomExercises: {
    flex: 1,
    marginTop: 10,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
    paddingLeft: 20,
    marginVertical: 7,
    marginHorizontal: ScreenWidth(0.05),
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.39,
    shadowRadius: 8.3,
    elevation: 3,
  },
  itemImage: {
    width: ScreenWidth(0.2),
    height: ScreenWidth(0.15),
  },
  itemText: {
    marginLeft:'2%',
    fontSize: 21,
  },

  linearGradient: {
    borderRadius: 30,
    padding: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  containerPlusButton: {
    position: 'absolute',
    bottom: ScreenWidth(0.2),
    right: 10,
    zIndex: 2,
  },
  addTextView: {
    paddingHorizontal: 10,
  },
  addText: {
    fontSize: 24,
    color: Colors.white,
  },
  plusButton: {
    width: 36,
    height: 36,
    borderRadius: 18,
    backgroundColor: Colors.lightBlue,
    justifyContent: 'center',
    alignItems: 'center',
  },
  minusButton: {
    backgroundColor: Colors.lightBlue,
    position: 'absolute',
    right: 10,
    zIndex: 2,
    width: 40,
    height: 40,
    borderRadius: 20,
  },
  itemIcon: {
    marginTop:'15%',
    textAlign:'center',
    fontSize: 26,
    color: Colors.white,
  },
});

export default styles;
