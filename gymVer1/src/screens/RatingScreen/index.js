import React, { useState, useEffect } from 'react';
import { Text, View, TouchableOpacity, TextInput } from 'react-native';
import FastImage from 'react-native-fast-image';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import { showMessage } from 'react-native-flash-message';
import LinearGradient from 'react-native-linear-gradient';

import styles from './styles';
import Images from '../../common/Images';
import HeaderActionScreen from '../../components/HeaderActionScreen';
import HideKeyboard from '../../components/HideKeyboard';
import Colors from '../../common/Colors';
import {
  getRates,
  addNewRating,
  getRateOfPTAboutRoom,
  addNewRateOfPT,
} from '../../actions';
import AsyncStorageService from '../../api/AsyncStorageService';

const listStars = [1, 2, 3, 4, 5];

const MyRateScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const rateReducer = useSelector((state) => state.rate);
  const authReducer = useSelector((state) => state.auth);
  const homeReducer = useSelector((state) => state.home);

  const { role, info } = authReducer;
  const { dataOnHomeOfTrainee } = homeReducer;
  const { ptId } = dataOnHomeOfTrainee;


  const [ratingRoom, setRatingRoom] = useState(4);
  const [contentRatingRoom, setContentRatingRoom] = useState('');
  const [idRatingRoom, setIdRatingRoom] = useState(null);
  const [ratingPT, setRatingPT] = useState(4);
  const [contentRatingPT, setContentRatingPT] = useState('');

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      if (role?.length > 0 && role[0].name === 'ROLE_PT') {
        getRateOfPT();
      } else {
        getRate();
      }
    });

    return unsubscribe;
  }, [navigation]);

  const getRate = async () => {
    await AsyncStorageService.putUser(`${info.id}`);
    dispatch(
      getRates({
        onSuccess: (data) => {
          const { ratePTDTO, rateCenterDTO } = data;
          if (ratePTDTO?.id) {
            setRatingPT(ratePTDTO.rateStar);
            setContentRatingPT(ratePTDTO.comment);
          }
          if (rateCenterDTO?.id) {
            setRatingRoom(rateCenterDTO.rate_star);
            setContentRatingRoom(rateCenterDTO.comment);
          }
        },
      }),
    );
  };

  const getRateOfPT = async () => {
    await AsyncStorageService.putUser(`${info.id}`);
    dispatch(
      getRateOfPTAboutRoom({
        onSuccess: (data) => {
          setRatingRoom(data.rate_star);
          setContentRatingRoom(data.comment);
          setIdRatingRoom(data.id);
        },
      }),
    );
  };

  const sendRatingOfPT = async () => {
    if (contentRatingRoom.length === 0) {
      showMessage({
        message: 'Please input a rating',
        type: 'warning',
        duration: 2000,
      });
    } else {
      let payload;
      if (idRatingRoom) {
        payload = {
          id: idRatingRoom,
          comment: contentRatingRoom,
          rate_star: ratingRoom,
        };
      } else {
        payload = {
          comment: contentRatingRoom,
          rate_star: ratingRoom,
        };
      }
      dispatch(addNewRateOfPT(payload));
    }
  };

  const sendRating = async () => {
    console.log("Đánh giá phòng tập và PT")
    if (contentRatingRoom.length === 0 || contentRatingPT.length === 0) {
      showMessage({
        message: 'Please input a rating',
        type: 'warning',
        duration: 2000,
      });
    } else {
      const { ratePTDTO, rateCenterDTO } = rateReducer.data;
      let payload;

      // if (ratePTDTO?.id && rateCenterDTO?.id) {
      payload = {
        ratePTDTO: {
          id: ratePTDTO?.id || null,
          comment: contentRatingPT,
          rateStar: ratingPT,
        },
        rateCenterDTO: {
          id: rateCenterDTO?.id || null,
          comment: contentRatingRoom,
          rate_star: ratingRoom,
        },
      };
      dispatch(addNewRating(payload));
    }
  };

  if (role?.length > 0 && ptId == null) {
    return (
      <HideKeyboard>
        <View style={styles.container}>
          <HeaderActionScreen
            title="Rating"
            openDrawer={() => navigation.openDrawer()}
            sizeIconDrawer={20}
          />

          <View style={styles.reportItem}>
            <View style={styles.headerItem}>
              <FastImage
                source={Images.defaultAvatar}
                style={styles.userImgItem}
              />
              <Text style={styles.userNameItem}>{info?.full_name}</Text>
            </View>
            <TextInput
              placeholder="Write your rating room"
              multiline
              style={styles.ratingInput}
              value={contentRatingRoom}
              onChangeText={(text) => setContentRatingRoom(text)}
            />
            <View style={styles.starsItem}>
              {listStars.map((star) => (
                <View key={star} style={styles.starItem}>
                  <TouchableOpacity
                    key={star}
                    activeOpacity={0.8}
                    onPress={() => setRatingRoom(star)}>
                    <FastImage
                      source={
                        ratingRoom >= star
                          ? Images.activeStar
                          : Images.inActiveStar
                      }
                      style={styles.starImageItem}
                    />
                  </TouchableOpacity>
                </View>
              ))}
            </View>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.containerPlusButton}
            onPress={sendRatingOfPT}>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 0, y: 1 }}
              colors={Colors.mainGradientButton}
              style={styles.linearGradient}>
              <Text style={styles.addText}>Send</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </HideKeyboard>
    );
  } else if (role?.length > 0 && role[0].name === 'ROLE_TRAINEE') {
    console.log("INFO: ", info)
    return (
      <HideKeyboard>
        <View style={styles.container}>
          <HeaderActionScreen
            title=""
            openDrawer={() => navigation.openDrawer()}
            sizeIconDrawer={20}
          />

          <View style={styles.reportItem}>
            <View style={styles.headerItem}>
              <FastImage
                source={Images.defaultAvatar}
                style={styles.userImgItem}
              />
              <Text style={styles.userNameItem}>Rating Center</Text>
            </View>
            <TextInput
              placeholder="Write your rating center"
              multiline
              style={styles.ratingInput}
              value={contentRatingRoom}
              onChangeText={(text) => setContentRatingRoom(text)}
            />
            <View style={styles.starsItem}>
              {listStars.map((star) => (
                <View key={star} style={styles.starItem}>
                  <TouchableOpacity
                    key={star}
                    activeOpacity={0.8}
                    onPress={() => setRatingRoom(star)}>
                    <FastImage
                      source={
                        ratingRoom >= star
                          ? Images.activeStar
                          : Images.inActiveStar
                      }
                      style={styles.starImageItem}
                    />
                  </TouchableOpacity>
                </View>
              ))}
            </View>
          </View>

          <View style={styles.reportItem}>
            <View style={styles.headerItem}>
              <FastImage
                source={Images.defaultAvatar}
                style={styles.userImgItem}
              />
              <Text style={styles.userNameItem}>Rating Your PT</Text>
            </View>
            <TextInput
              placeholder="Write your rating PT"
              multiline
              style={styles.ratingInput}
              value={contentRatingPT}
              onChangeText={(text) => setContentRatingPT(text)}
            />
            <View style={styles.starsItem}>
              {listStars.map((star) => (
                <View key={star} style={styles.starItem}>
                  <TouchableOpacity
                    key={star}
                    activeOpacity={0.8}
                    onPress={() => setRatingPT(star)}>
                    <FastImage
                      source={
                        ratingPT >= star
                          ? Images.activeStar
                          : Images.inActiveStar
                      }
                      style={styles.starImageItem}
                    />
                  </TouchableOpacity>
                </View>
              ))}
            </View>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.containerPlusButton}
            onPress={sendRating}>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 0, y: 1 }}
              colors={Colors.mainGradientButton}
              style={styles.linearGradient}>
              <Text style={styles.addText}>Send</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </HideKeyboard>
    );
  } else {
    return <View />;
  }
};

export default MyRateScreen;
