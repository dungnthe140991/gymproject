import React, { useState, useEffect } from 'react';
import { Text, View, TouchableOpacity, FlatList } from 'react-native';
import FastImage from 'react-native-fast-image';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import moment from 'moment';

import styles from './styles';
import Images from '../../common/Images';
import HeaderActionScreen from '../../components/HeaderActionScreen';
import { getRates, getRatesOfPT } from '../../actions';
import AsyncStorageService from '../../api/AsyncStorageService';

const listStars = [1, 2, 3, 4, 5];

const ReportsScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const rateReducer = useSelector((state) => state.rate);
  const authReducer = useSelector((state) => state.auth);

  const { role, info } = authReducer;

  const { ratePTDTO, rateCenterDTO } = rateReducer.data;
  const { dataOfPT } = rateReducer;

  const [rating, setRating] = useState(4);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getRate();
    });
    return unsubscribe;
  }, [navigation]);


  const getRate = async () => {
    if (role?.length > 0 && role[0].name === 'ROLE_PT') {
      dispatch(getRatesOfPT({ page: 0, size: 10000000 }));
    } else {
      dispatch(getRates());
    }
  };

  const renderItemReport = ({ item }) => {
    console.log("ITEM: ", item)
    return (
      <View style={styles.reportItem}>
        <View style={styles.headerItem}>
          <FastImage source={Images.defaultAvatar} style={styles.userImgItem} />
          <Text style={styles.userNameItem}>Học viên ẩn danh</Text>
        </View>
        <View style={styles.starsItem}>
          {listStars.map((star) => (
            <View key={star} style={styles.starItem}>
              <View key={star}>
                <FastImage
                  source={
                    item.starRate >= star
                      ? Images.activeStar
                      : Images.inActiveStar
                  }
                  style={styles.starImageItem}
                />
              </View>
            </View>
          ))}
        </View>
        <Text style={styles.createAtText}>{moment(item?.created_date).format('DD/MM/YYYY')}</Text>
        <Text style={styles.contentItem}>{item.comment}</Text>
      </View>
    );
  };
  const { startRate, traineeRatePTResource } = dataOfPT;
  console.log(rateReducer.data, traineeRatePTResource);

  return (
    <View style={styles.container}>
      <HeaderActionScreen
        title="Report"
        openDrawer={() => navigation.openDrawer()}
        sizeIconDrawer={20}
      />

      <View style={styles.wrapStar}>
        <View style={styles.stars}>
          {listStars.map((star) => (
            <View key={star} style={styles.star}>
              <View key={star}>
                <FastImage
                  source={
                    startRate >= star ? Images.activeStar : Images.inActiveStar
                  }
                  style={styles.starImage}
                />
              </View>
            </View>
          ))}
          <Text>
            {startRate && (Math.round(startRate * 100) / 100).toFixed(1)}
          </Text>
        </View>
      </View>

      {traineeRatePTResource?.content && (
        <FlatList
          style={styles.listReports}
          data={traineeRatePTResource.content}
          keyExtractor={(item, indx) => `${indx}`}
          renderItem={renderItemReport}
          showsVerticalScrollIndicator={false}
        />
      )}
      {rateCenterDTO && role?.length > 0 && role[0].name === 'ROLE_TRAINEE' ? (
        <View style={styles.boxShadow}>
          <View style={styles.reportItem}>
            <View style={styles.headerItem}>
              <FastImage
                source={Images.defaultAvatar}
                style={styles.userImgItem}
              />
              <Text style={styles.userNameItem}>Room Rating</Text>
            </View>
            <Text style={styles.contentItem}>{rateCenterDTO.comment}</Text>
            <View style={styles.starsItem2}>
              {listStars.map((star) => (
                <View key={star} style={styles.starItem2}>
                  <View key={star}>
                    <FastImage
                      source={
                        rateCenterDTO.rate_star >= star
                          ? Images.activeStar
                          : Images.inActiveStar
                      }
                      style={styles.starImageItem2}
                    />
                  </View>
                </View>
              ))}
            </View>
          </View>
        </View>
      ) : (
        <View style={styles.emptyViewRating}>
          {role?.length > 0 && role[0].name === 'ROLE_TRAINEE' && (
            <Text style={styles.emptyTextRating}>Rating is empty</Text>
          )}
        </View>
      )}
      {ratePTDTO && role?.length > 0 && role[0].name === 'ROLE_TRAINEE' && (
        <View style={styles.boxShadow}>
          <View style={styles.reportItem}>
            <View style={styles.headerItem}>
              <FastImage
                source={Images.defaultAvatar}
                style={styles.userImgItem}
              />
              <Text style={styles.userNameItem}>PT Rating</Text>
            </View>
            <Text style={styles.contentItem}>{ratePTDTO.comment}</Text>
            <View style={styles.starsItem2}>
              {listStars.map((star) => (
                <View key={star} style={styles.starItem2}>
                  <View key={star}>
                    <FastImage
                      source={
                        ratePTDTO.rateStar >= star
                          ? Images.activeStar
                          : Images.inActiveStar
                      }
                      style={styles.starImageItem2}
                    />
                  </View>
                </View>
              ))}
            </View>
          </View>
        </View>
      )}
    </View>
  );
};

export default ReportsScreen;
