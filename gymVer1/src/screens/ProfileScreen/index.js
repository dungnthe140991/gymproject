import React, { useRef, useState, useEffect } from 'react';
import { Text, SafeAreaView, TouchableOpacity, Alert, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { launchImageLibrary } from 'react-native-image-picker';
import MCIIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FAIcons from 'react-native-vector-icons/FontAwesome';

import styles from './styles';
import HeaderActionScreen from '../../components/HeaderActionScreen';
import InputCustom from '../../components/InputCustom';
import HideKeyboard from '../../components/HideKeyboard';
import Avatar from '../../components/Avatar';
import Colors from '../../common/Colors';
import Constants from '../../common/Constants';
import Validator from '../../utils/Validator';
import { updateInfo, uploadImage } from '../../actions';
import AsyncStorageService from '../../api/AsyncStorageService';

const ProfileScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const authReducer = useSelector((state) => state.auth);
  const { info, role } = authReducer;

  const nameValue = info?.full_name || '';
  const [loading, setLoading] = useState(false);
  const [image, setImage] = useState(null);
  const [error, setError] = useState({ name: null, phone: null, email: null });
  const [payload, setPayload] = useState({});

  const refName = useRef(null);
  const refEmail = useRef(null);
  const refPhone = useRef(null);

  useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
      setLoading(true);
      setTimeout(() => {
        setLoading(false);
      }, 700);
    });

    return unsubscribe;
  }, [navigation]);

  const createFormData = (photo) => {
    const data = new FormData();

    data.append('file', {
      name: Constants.isIOS ? photo.filename : `${new Date()}.png`,
      type: Constants.isIOS ? 'jpeg' : 'image/jpeg',
      uri: Constants.isIOS ? photo?.uri.replace('file://', '') : photo.uri,
    });

    return data;
  };

  const onChangeImage = async () => {
    const result = await launchImageLibrary({
      selectionLimit: 1,
    });

    if (result.didCancel || result.errorCode) {
      return;
    }
    if (result?.assets.length === 1) {
      setImage(result.assets[0]);
    }
  };

  const editInfoUser = async () => {
    const name = refName.current.value;
    const email = refEmail.current.value;
    const phone = refPhone.current.value;

    const err = { name: null, phone: null, email: null };
    let nameStatus = false;
    let emailStatus = false;
    let phoneStatus = false;

    if (Validator.isEmpty(name)) {
      err.name = 'Name is required';
      nameStatus = true;
    }

    if (Validator.isEmpty(email)) {
      err.email = 'Email is required';
      emailStatus = true;
    }
    if (!emailStatus && !Validator.isEmail(email)) {
      err.email = 'Email is not corrected format';
      emailStatus = true;
    }

    if (Validator.isEmpty(phone)) {
      err.phone = 'Phone is required';
      phoneStatus = true;
    }
    if (!phoneStatus && !Validator.isPhoneNumber(phone)) {
      err.phone = 'Phone is not corrected format';
      phoneStatus = true;
    }

    setError(err);

    if (!nameStatus && !emailStatus && !phoneStatus) {
      await AsyncStorageService.putUser(`${info.id}`);
      Alert.alert('Notice', 'Do you want to change?', [
        {
          text: 'Ok',
          onPress: () => {
            if (image?.uri) {
              const formDataAvatar = createFormData(image);
              dispatch(
                uploadImage(formDataAvatar, {
                  onSuccess: (url) => {
                    if (role[0].name === 'ROLE_TRAINEE') {
                      const payload = {
                        // id: info.id,
                        phone: refPhone.current.value,
                        name: refName.current.value,
                        avatar: url,
                      };
                      if (role?.length > 0) {
                        dispatch(updateInfo(payload, role[0].name));
                        setImage(url);
                      }
                    } else {
                      const payload = {
                        id: info.id,
                        phone: refPhone.current.value,
                        full_name: refName.current.value,
                        avatar: url,
                      };
                      if (role?.length > 0) {
                        dispatch(updateInfo(payload, role[0].name));
                        setImage(url);
                      }
                    }
                  },
                }),
              );
            } else {
              if (role[0].name === 'ROLE_TRAINEE') {
                const payload = {
                  // id: info.id,
                  phone: refPhone.current.value,
                  name: refName.current.value,
                  avatar: info.avatar,
                };
                if (role?.length > 0) {
                  dispatch(updateInfo(payload, role[0].name));
                  setImage(info.avatar);
                }
              } else {
                const payload = {
                  id: info.id,
                  phone: refPhone.current.value,
                  full_name: refName.current.value,
                  avatar: info.avatar,
                };
                if (role?.length > 0) {
                  dispatch(updateInfo(payload, role[0].name));
                  setImage(info.avatar);
                }
              }
              // if (role?.length > 0) {
              //   console.log("VAO ELSE");
              //   dispatch(updateInfo(payload, role[0].name));
              //   setImage(info.avatar);
              // }
            }
          },
        },
        { text: 'Cancel', onPress: () => { } },
      ]);
    }
  };

  const focusInput = (type) => {
    switch (type) {
      case 'name':
        setError({ ...error, name: null });
        break;
      case 'phone':
        setError({ ...error, phone: null });
        break;
      case 'email':
        setError({ ...error, email: null });
        break;

      default:
        break;
    }
  };

  const changePassword = () => {
    navigation.navigate('ChangePasswordScreen');
  };

  return (
    <HideKeyboard>
      <SafeAreaView style={styles.container}>
        <HeaderActionScreen
          title="Profile"
          openDrawer={() => navigation.openDrawer()}
          sizeIconDrawer={20}
        />
        <Avatar
          onChangeImage={onChangeImage}
          editable
          image={{ uri: info.avatar }}
        />
        {!loading && (
          <>
            <InputCustom
              id="name"
              ref={refName}
              placeholder="Full Name"
              iconRight={<FAIcons name="user" style={styles.inputIcon} />}
              containerStyleProps={styles.inputContainer}
              useFocusInput
              hideIconLeft
              valueInput={info?.fullname!=null?info?.fullname:info?.full_name}
              errorText={error.name}
              focusAction={focusInput}
            />
            <InputCustom
              id="email"
              ref={refEmail}
              placeholder="Email Address"
              iconRight={<MCIIcons name="email" style={styles.inputIcon} />}
              containerStyleProps={styles.inputContainer}
              useFocusInput
              hideIconLeft
              editable={false}
              valueInput={info?.email}
              keyboardType="email-address"
              errorText={error.email}
              focusAction={focusInput}
            />
            <InputCustom
              id="phone"
              ref={refPhone}
              placeholder="Phone Number"
              iconRight={<MCIIcons name="phone" style={styles.inputIcon} />}
              containerStyleProps={styles.inputContainer}
              useFocusInput
              hideIconLeft
              valueInput={info?.phone}
              keyboardType="decimal-pad"
              errorText={error.phone}
              focusAction={focusInput}
            />
          </>
        )}

        <View style={styles.actionView}>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.containerPlusButton}
            onPress={editInfoUser}>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 0, y: 1 }}
              colors={Colors.mainGradientButton}
              style={styles.linearGradient}>
              <Text style={styles.addText}>Save</Text>
            </LinearGradient>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.containerPlusButton}
            onPress={changePassword}>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 0, y: 1 }}
              colors={Colors.mainGradientButton}
              style={styles.linearGradient}>
              <Text style={styles.addText}>Change Password</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </HideKeyboard>
  );
};

export default ProfileScreen;
