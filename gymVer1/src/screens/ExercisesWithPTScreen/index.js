import React from 'react';
import {
  SafeAreaView,
  Text,
  View,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import AntDIcons from 'react-native-vector-icons/AntDesign';

import styles from './styles';
import Colors from '../../common/Colors';
import Images from '../../common/Images';
import HeaderActionScreen from '../../components/HeaderActionScreen';
import {getDetailExercise, removeExercise} from '../../actions';

const ExercisesWithPTScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const exercisesReducer = useSelector((state) => state.exercises);

  const {myExercises} = exercisesReducer;

  const goToTypeOfExercises = (item) => {
    dispatch(
      getDetailExercise(item.id, {
        onSuccess: (data) => {
          navigation.navigate('DetailRoomExercisesScreen', {
            data,
            showButtonAdd: true,
            title: item.title,
            parentId: item.id,
            from: 'ExercisesWithPTScreen',
          });
        },
        onFailure: () => {},
      }),
    );
  };

  const renderItemExercises = ({item}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={[styles.item, styles.bgGray]}
        onPress={() => goToTypeOfExercises(item)}>
        <Text style={styles.itemText}>{item?.title || item?.name}</Text>
        {item?.picture?.length > 5 || item?.image?.length > 5 ? (
          item?.image?.length > 5 ? (
            <FastImage style={styles.itemImage} source={{uri: item.image}} />
          ) : (
            <FastImage style={styles.itemImage} source={Images.chestMuscles} />
          )
        ) : (
          <FastImage style={styles.itemImage} source={Images.chestMuscles} />
        )}
      </TouchableOpacity>
    );
  };

  const headerComponent = () => {
    return (
      <View style={styles.headerContainer}>
        <View style={styles.sttTitleTable}>
          <Text style={styles.textTitleTable}>Date</Text>
        </View>
        <View style={styles.nameTitleTable}>
          <Text style={styles.textTitleTable}>Time</Text>
        </View>
        <View style={styles.phoneTitleTable}>
          <Text style={styles.textTitleTable}>Exercises</Text>
        </View>
      </View>
    );
  };

  const renderItemSeparatorComponent = () => {
    return <View style={styles.itemSeparatorComponent} />;
  };

  console.log('myExercises', myExercises)

  return (
    <SafeAreaView style={styles.container}>
      <HeaderActionScreen
        title="Exercises"
        goBack={() => navigation.goBack()}
        sizeIconBack={28}
      />

      <FlatList
        data={myExercises}
        keyExtractor={(item) => `${item.id}`}
        style={styles.listRoomExercises}
        ListFooterComponent={<View style={styles.emptyItemView} />}
        renderItem={renderItemExercises}
        ListHeaderComponent={headerComponent}
        ItemSeparatorComponent={renderItemSeparatorComponent}
      />
    </SafeAreaView>
  );
};

export default ExercisesWithPTScreen;
