import React, { useState, useRef } from 'react';

import {
  SafeAreaView,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { showMessage } from 'react-native-flash-message';
import { launchImageLibrary } from 'react-native-image-picker';
import AntDIcons from 'react-native-vector-icons/AntDesign';

import styles from './styles';
import { listDetailRoomExercises } from '../../common/Config';
import HeaderActionScreen from '../../components/HeaderActionScreen';
import { addDoingExercise } from '../../actions';

const AddDoingExercisesScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const dispatch = useDispatch();

  const title = route.params?.title || '';

  const modalRef = useRef(null);
  const richtext = useRef(null);

  const [image, setImage] = useState(null);
  const [text, setText] = useState('');
  const [steps, setSteps] = useState([]);
  const [idType, setIdType] = useState(listDetailRoomExercises[0].id);
  const [nameType, setNameType] = useState(listDetailRoomExercises[0].name);
  const [content, setContent] = useState(route.params.data.content);

  // const chooseImage = async () => {
  //   const result = await launchImageLibrary({
  //     selectionLimit: 1,
  //   });

  //   if (result.didCancel || result.errorCode) {
  //     return;
  //   }
  //   if (result?.assets.length === 1) {
  //     setImage(result.assets[0]);
  //   }
  // };

  // const chooseType = () => {
  //   modalRef.current.openModal();
  // };

  // const getDetailExercises = (value) => {
  //   setIdType(value.id);
  //   setNameType(value.name);
  // };

  // const addNewStep = () => {
  //   if (image?.uri && text.length > 0) {
  //     setSteps([
  //       ...steps,
  //       {
  //         title: text,
  //         image: image.uri,
  //       },
  //     ]);
  //     setImage(null);
  //     setText('');
  //   } else {
  //     showMessage({
  //       message: 'Hãy điền đủ thông tin',
  //       type: 'warning',
  //       duration: 2000,
  //     });
  //   }
  // };

  const add = () => {
    const id = route.params.id;
    console.log("ID:", id);
    console.log("CONTENT:", content)
    dispatch(
      addDoingExercise(
        id,
        { content: content },
        {
          onSuccess: (data) => {
            console.log("DATA AFTER: ",data)
            navigation.goBack();
          },
        },
      ),
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.headerAction}>
        <HeaderActionScreen
          goBack={() => navigation.goBack()}
          sizeIconBack={30}
          title={title}
          colorText="red"
        />
      </View>
      <View style={styles.textAreaContainer} >
        <TextInput
          style={styles.textArea}
          underlineColorAndroid="transparent"
          placeholder="Type something"
          placeholderTextColor="grey"
          numberOfLines={10}
          multiline={true}
          onChangeText={(val) => setContent(val)}
          // value={content}
          value={content}
        />
      </View>
      <View style={styles.containerPlusButton}>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.plusButton}
          onPress={add}>
          <Text style={styles.btnSaveText}>Save</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default AddDoingExercisesScreen;
