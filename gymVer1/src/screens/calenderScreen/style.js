import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  itemContainer: {
    width: '100%',
    backgroundColor: 'white',
    flexDirection: 'row',
    marginVertical: 5,
    borderRadius: 10,
  },
  timeContainer: {
    width: '50%',
    backgroundColor: 'white',
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  trainerContainer: {
    width: '50%',
    backgroundColor: 'white',
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  headerContainer: {
    width: '100%',
    backgroundColor: 'white',
    flexDirection: 'row',
    marginVertical: 5,
    borderRadius: 10,
  },
  textSlotContainer: {
    width: '50%',
    backgroundColor: 'white',
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  textTrainessContainer: {
    width: '50%',
    backgroundColor: 'white',
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  textTitleColumn: {
    fontWeight: 'bold',
  },
  emptyContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  bannerContainer: {
    width: '100%',
    height: '100%',
    position: 'absolute',
  },
  textContainer: {
    width: '100%',
    height: 150,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'white',
  },
  bodyContainer: {
    width: '100%',
    flex: 1,
    backgroundColor: 'white',
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    paddingVertical: 40,
  },
  contentContainer: {
    overflow: 'hidden',
    paddingBottom: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  calendarContainer: {
    shadowColor: '#000',
    shadowOffset: {},
    shadowOpacity: 0.4,
    shadowRadius: 4,
    elevation: 5,
    width: '100%',
    backgroundColor: '#fff',
  },
  listContainer: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    width: '80%',
    flex: 1,
    marginTop: 50,
  },
  itemSeparatorComponent: {
    height: 0.5,
    width: '100%',
    backgroundColor: Colors.border,
  },
});

export default styles;
