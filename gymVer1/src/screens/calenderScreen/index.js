/* eslint-disable react-hooks/exhaustive-deps */
import {Text, View, ScrollView, FlatList, TouchableOpacity} from 'react-native';
import React, {useMemo, useState, useCallback, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation, useRoute} from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import {LocaleConfig, Calendar} from 'react-native-calendars';
LocaleConfig.locales.vi = {
  monthNames: [
    'Tháng 1',
    'Tháng 2',
    'Tháng 3',
    'Tháng 4',
    'Tháng 5',
    'Tháng 6',
    'Tháng 7',
    'Tháng 8',
    'Tháng 9',
    'Tháng 10',
    'Tháng 11',
    'Tháng 12',
  ],
  monthNamesShort: [
    'T.1',
    'T.2',
    'T.3',
    'T.4',
    'T.5',
    'T.6',
    'T.7',
    'T.8',
    'T..',
    'T.10',
    'T.11',
    'T.12',
  ],
  dayNames: ['Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7', 'Chủ Nhật'],
  dayNamesShort: ['Th.2', 'Th.3', 'Th.4', 'Th.5', 'Th.6', 'Th.7', 'C.N'],
};
LocaleConfig.defaultLocale = 'vi';
import styles from './style';
import {
  getCalendarTrainersByDate,
  getExercisesByTrainerId,
  clearDataTrainers,
  getPriceByParams,
} from '../../actions';
import Colors from '../../common/Colors';
import {tempData} from '../../common/tempData';
import HeaderActionScreen from '../../components/HeaderActionScreen';
import {formatDate1, formatTodayByYMD} from '../../utils/DateFormat';
import moment from 'moment';

const CalenderScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const dispatch = useDispatch();
  const authReducer = useSelector((state) => state.auth);
  const calendarReducer = useSelector((state) => state.calendar);
  const {info} = authReducer;
  const {trainers} = calendarReducer;

  const from = route.params?.from || '';
  const [selected, setSelected] = useState();
  
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      dispatch(clearDataTrainers());
    });
    return unsubscribe;
  }, [navigation]);

  const onCalendarPress = useCallback((day) => {
    const formatDate = formatDate1(day); 
    setSelected(day);

    if (from) {
      tempData.selectDate = day;
      if (tempData.packageDate) {
        dispatch(getPriceByParams(tempData.packageDate, day));
      }
      navigation.goBack();
    } else {
      dispatch(getCalendarTrainersByDate(info?.id, formatDate));
    }
  }, []);

  const marked = useMemo(() => {
    return {
      [selected]: {
        selected: true,
        disableTouchEvent: true,
        selectedColor: Colors.primary,
        selectedTextColor: Colors.white,
      },
      ['2022-07-22']: {
        dotColor: 'red',
        marked: true,
      },
    };
  }, [selected]);

  const navExercisesOfUs = (item) => {
    var dateNow = new Date();
    dateNow.setDate(1);
    var dateTo = new Date();
    dateTo.setDate(dateNow.getDate() + 30);
    dispatch(getExercisesByTrainerId(item.id,
      moment(dateNow).format('YYYY-MM-DD'),
      moment(dateTo).format('YYYY-MM-DD')));


    navigation.navigate('ExercisesOfTraineeScreen', {
      traineeId: item.id,
      traineeName: item.name,
    });
  };

  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => navExercisesOfUs(item)}
        style={styles.itemContainer}>
        <View style={styles.timeContainer}>
          <Text>{item?.slotName}</Text>
        </View>
        <View style={styles.trainerContainer}>
          <Text>{item?.name}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  const headerComponent = () => {
    return (
      <View style={styles.headerContainer}>
        <View style={styles.textSlotContainer}>
          <Text style={styles.textTitleColumn}>SLOT</Text>
        </View>
        <View style={styles.textTrainessContainer}>
          <Text style={styles.textTitleColumn}>TRAINERS</Text>
        </View>
      </View>
    );
  };

  const renderEmptyContainer = () => {
    return (
      <View style={styles.emptyContainer}>
        <Text> NO DATA </Text>
      </View>
    );
  };

  const renderItemSeparatorComponent = () => {
    return <View style={styles.itemSeparatorComponent} />;
  };
  
  // onCalendarPress(new Date().toDateString());

  return (
    <View style={styles.container}>
      <LinearGradient
        colors={['#8736e7', '#b732a0']}
        start={{x: 0, y: 1}}
        end={{x: 1, y: 1}}
        style={{width: '100%', height: 300}}
      />
      <View style={styles.bannerContainer}>
        {from ? (
          <HeaderActionScreen
            goBack={() => navigation.goBack()}
            sizeIconBack={30}
            title=""
          />
        ) : null}
        <View style={styles.textContainer}>
          <Text style={styles.textStyle}>
            {from ? 'Select Registration Date' : 'Select Date'}
          </Text>
        </View>
        <ScrollView style={styles.bodyContainer}>
          <View style={styles.contentContainer}>
            <View style={styles.calendarContainer}>
              <Calendar
                // Initially visible month. Default = now
                current={formatTodayByYMD()}
                minDate={'2012-05-10'}
                maxDate={'2100-05-30'}
                onDayPress={(day) => {
                  onCalendarPress(day.dateString);
                  console.log("LOG 1: ",day.dateString)
                }}
                onDayLongPress={(day) => {
                  onCalendarPress(day.dateString);
                }}
                monthFormat={'MM/yyyy'}
                onMonthChange={(month) => {
                  console.log('month changed', month);
                }}
                firstDay={1}
                hideDayNames={true}
                showWeekNumbers={true}
                onPressArrowLeft={(subtractMonth) => subtractMonth()}
                onPressArrowRight={(addMonth) => addMonth()}
                disableArrowLeft={false}
                disableArrowRight={false}
                enableSwipeMonths={false}
                markedDates={marked}
              />
            </View>
            {!from && (
              <View style={styles.listContainer}>
                <FlatList
                  data={trainers}
                  renderItem={renderItem}
                  keyExtractor={(item) => `${item.id}`}
                  ListHeaderComponent={headerComponent}
                  ListEmptyComponent={renderEmptyContainer}
                  ItemSeparatorComponent={renderItemSeparatorComponent}
                />
              </View>
            )}
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default CalenderScreen;
