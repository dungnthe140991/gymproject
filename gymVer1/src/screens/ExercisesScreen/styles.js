import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.sectionSeparatorColor,
    alignItems: 'center',
  },
  scrollContainer: {
    alignItems: 'center',
  },

  imageLogo: {
    marginTop: 40,
    marginBottom: 20,
    width: ScreenWidth(0.6),
    height: ScreenWidth(0.42),
  },

  exercisesView: {
    width: ScreenWidth(0.86),
    height: ScreenWidth(0.42),
    borderRadius: 25,
    position: 'relative',
    marginTop: 30,
  },
  exercisesImage: {
    flex: 1,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  exercisesText: {
    fontSize: 25,
    fontWeight: 'bold',
    color: Colors.white,
    zIndex: 2,
  },
  layout: {
    backgroundColor: Colors.rgba3,
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    zIndex: 1,
  },
  emptyView: {
    height: 50,
    width: 60,
  },
});

export default styles;
