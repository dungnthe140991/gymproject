import React from 'react';
import {SafeAreaView, Text, View, FlatList} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useNavigation, useRoute} from '@react-navigation/native';

import styles from './styles';
import Images from '../../common/Images';
import HeaderActionScreen from '../../components/HeaderActionScreen';

const DetailDoingExerciseScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();

  const title = route.params?.title || '';
  const content = route.params?.data?.content || [];

  const renderItemDetailExercises = ({item}) => {
    console.log('ITEM: ',item)
    return (
      <View>
        <Text style={styles.stepTitleText}>{item.title}</Text>
        <Text style={styles.stepText}>{item.content}</Text>
        <FastImage style={styles.imageStep} source={{ uri: item.image }} />
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <HeaderActionScreen
        goBack={() => navigation.goBack()}
        sizeIconBack={30}
        title={title}
        colorText="red"
      />
      <FlatList
        data={content}
        keyExtractor={(item) => `${item.id}`}
        style={styles.listRoomExercises}
        renderItem={renderItemDetailExercises}
      />
    </SafeAreaView>
  );
};

export default DetailDoingExerciseScreen;
