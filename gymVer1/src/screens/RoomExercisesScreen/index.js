import React from 'react';
import { SafeAreaView, Text, FlatList, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import AntDIcons from 'react-native-vector-icons/AntDesign';
import moment from 'moment';

import styles from './styles';
import Images from '../../common/Images';
import HeaderActionScreen from '../../components/HeaderActionScreen';
import {
  getDetailExercise,
  assignExercisesForTrainee,
  getExercisesByTrainerId,
  assignExercisesAndDateForTrainee,
} from '../../actions';

const RoomExercisesScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const exercisesReducer = useSelector((state) => state.exercises);
  const route = useRoute();

  const assign = route.params?.assign || false;
  const traineeId = route.params?.traineeId || 0;
  const calendarDate = route.params?.calendarDate || 0;

  const { exercises, myExercises } = exercisesReducer;

  const goToTypeOfExercises = (item) => {
    if (assign) {
      const payload = {
        calendarDate: calendarDate,
        categoryId: item.id,
        traineeId: traineeId
      }
      dispatch(
        assignExercisesAndDateForTrainee(payload, {
          onSuccess: () => {
            var dateNow = new Date();
            dateNow.setDate(1);
            var dateTo = new Date();
            dateTo.setDate(dateNow.getDate() + 30);
        
            dispatch(getExercisesByTrainerId(traineeId, 
              moment(dateNow).format('YYYY-MM-DD'), 
              moment(dateTo).format('YYYY-MM-DD')));
            setTimeout(() => {
              navigation.goBack();
            }, 500);
          },
          onFailure: () => {
            setTimeout(() => {
              navigation.goBack();
            }, 300);
          },
        }),
      );
    } else {
      dispatch(
        getDetailExercise(item.id, {
          onSuccess: (data) => {
            navigation.navigate('DetailRoomExercisesScreen', {
              data,
              showButtonAdd: false,
              showButtonSub: false,
              title: item.title,
            });
          },
          onFailure: () => { },
        }),
      );
    }
  };

  const handleAddExercise = () => { };

  const renderItemExercises = ({ item }) => {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={[styles.item, styles.bgGray]}
        onPress={() => goToTypeOfExercises(item)}>
        <Text style={styles.itemText}>{item.title}</Text>
        {item?.image?.length > 0 ? (
          <FastImage style={styles.itemImage} source={{ uri: item.image }} />
        ) : (
          <FastImage style={styles.itemImage} source={Images.chestMuscles} />
        )}
        {/* <TouchableOpacity
          style={[styles.plusButton, styles.minusButton]}
          onPress={() => handleAddExercise(item.id)}>
          <AntDIcons name="plus" style={styles.itemIcon} />
        </TouchableOpacity> */}
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <HeaderActionScreen
        goBack={() => navigation.goBack()}
        sizeIconBack={28}
        title="Exercises"
      />

      <FlatList
        // data={exercises}
        data={assign === true ? myExercises : exercises}
        keyExtractor={(item) => `${item.id}`}
        style={styles.listRoomExercises}
        renderItem={renderItemExercises}
      />
    </SafeAreaView>
  );
};

export default RoomExercisesScreen;
